package lesson06;

import java.util.Map;
import java.util.TreeMap;

public class BeanComparator {
    public static <T extends Object> void compare(T object1, T object2) {
        Map<String,T> map1 = new TreeMap<>();
        map1 = ReflectionInterface.getObjectFieldsValues(object1);

        System.out.println(String.format("\n%10s %45s %45s %10s\n","Field","Object1","Object2","Match"));
        for (Map.Entry<String, T> entry : map1.entrySet()){
            boolean flag = true;
            Map<String,T> map2 = new TreeMap<>();
            map2 = ReflectionInterface.getObjectFieldsValues(object2);
            String key = entry.getKey();
            try {
                Comparable value1 = (Comparable)entry.getValue();
                Comparable value2 = (Comparable)map2.get(key);
                if (!value1.equals(value2)) {
                    flag = false;
                }
                System.out.println(String.format("%10s %45s %45s %10s", key, entry.getValue().toString(), map2.get(key), flag ? 'V' : 'X'));
            } catch(ClassCastException | NullPointerException e) {
                e.getMessage();
            }
        }
    }   
}
