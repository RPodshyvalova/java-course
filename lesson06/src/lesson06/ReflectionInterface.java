package lesson06;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public interface ReflectionInterface {
    public static <T extends Object> Map<String,T> getObjectFieldsValues(T object) {
        Map<String,T> map = new TreeMap<>();
        
        Class<? extends Object> classOfObject =  object.getClass();
        while (!classOfObject.equals(Object.class)) {
            Field[] fieldsObject = classOfObject.getDeclaredFields();
            List<Field> listFieldsObject  =  Arrays.asList(fieldsObject);
                for(Field field : listFieldsObject){        
                    Annotation annotation = field.getAnnotation(lesson06.IgnorFieldAnnotation.class);
                    if (annotation == null ) {
                        field.setAccessible(true);
                        try {
                            T value = (T) field.get(object);
                            map.put(field.getName(), value);
                        }catch (IllegalArgumentException | IllegalAccessException e) {
                            e.getMessage();
                        }
                    }
                }
            classOfObject = classOfObject.getSuperclass();
        } 
        return map;
    }
}
