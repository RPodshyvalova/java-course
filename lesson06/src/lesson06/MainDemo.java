package lesson06;

import javax.swing.JButton;

public class MainDemo {
    public static void main(String[] args) {
        Laptop laptopAcer = new Laptop("Acer Aspire E1-771G", "Intel Core i3-3120M(2.5GHz, 3MB L3 cache)", 8, 1000, "17.3 HD+LED LCD", "black");
        Laptop tabletAcer = new Laptop("Acer ICONIA Tab W500", "AMD C-60 Processor", 2, 32, "10.3", "gray");
        System.out.println(String.format("Object hashcode is %d;\t description = %s", laptopAcer.hashCode(), laptopAcer.toString()));
        System.out.println(String.format("Object hashcode is %d;\t description = %s", tabletAcer.hashCode(), tabletAcer.toString()));

        System.out.println("\nClone for object: " + tabletAcer.getClass().getName());
        Laptop laptopClone = CloneCreator.clone(tabletAcer);
        System.out.println(String.format("Object hashcode is %d;\t description = %s", tabletAcer.hashCode(), laptopClone.toString()));
        
        System.out.print("\n\n");
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("Let\'s compare laptopAcer, tabletAcer: ");
        BeanComparator.compare(laptopAcer, tabletAcer);
        System.out.print("\n\n");
        System.out.println("Let\'s compare laptopAcer, laptopAcer: ");
        BeanComparator.compare(laptopAcer, laptopAcer);
        
        System.out.print("\n\n");
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("Represente: " + laptopAcer.getClass().getName());
        BeenRepresenter.representer(laptopAcer);
        
        System.out.print("\n\n");
        System.out.println("------------------------------------------------------------------------------------");
        JButton button = new JButton();
        button.setText("Click me...");
        System.out.println("hashcode = " + button.hashCode() + "; description = " + button.toString());
        JButton buttonClone = CloneCreator.clone(button);
        System.out.println("hashcode = " + buttonClone.hashCode() + "; description = " + button.toString());
        System.out.print("\n\n");
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("Representation of the object: " + button.getClass().getName());
        BeenRepresenter.representer(button);
//        System.out.print("\n\n");
//        System.out.println("------------------------------------------------------------------------------------");
//        System.out.println("Compare: " + button.getClass().getName() );
//        BeanComparator.compare(button, button);

    }
}
