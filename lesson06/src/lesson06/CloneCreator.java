package lesson06;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.TreeMap;

public class CloneCreator {
    public static <T extends Object>T clone(T modelObject) {
        T cloneObject = null;
        Field field = null;
        Map<String,T> map = new TreeMap<>();
        map = ReflectionInterface.getObjectFieldsValues(modelObject);
        
        try {
            cloneObject = (T) modelObject.getClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.getMessage();
        }

        for(Map.Entry<String,T> entry : map.entrySet()){
            Class<? extends Object> classOfObject = cloneObject.getClass();
            try{
                field =  cloneObject.getClass().getDeclaredField(entry.getKey());
                field.setAccessible(true);
                field.set(cloneObject, (T)entry.getValue());

            } catch (IllegalArgumentException | NoSuchFieldException | IllegalAccessException e) {
                e.getMessage();
            }
                
        }
        return cloneObject;
    }
}