package lesson06;

public class Laptop {
    private String model;
    private String cpu;
    private int memorySize;
    private int hdd;
    private String display;
    @lesson06.IgnorFieldAnnotation
    private String color;
    
    Laptop(){
        model = "";
        cpu = ""; 
        memorySize = 0;
        hdd = 0;
        display = "";
        color = "";
    }
    
    Laptop(String model, String cpu, int memorySize, int hdd, String display, String color) {
        this.model = model;
        this.cpu = cpu; 
        this.memorySize = memorySize;
        this.hdd = hdd ;
        this.display = display;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Laptop[" + "model=" + model + ", cpu=" + cpu + ", memorySize=" + memorySize + ", hdd=" + hdd + ", display=" + display + ", color=" + color +']';
    }
}
