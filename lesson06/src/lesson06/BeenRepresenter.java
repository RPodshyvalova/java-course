package lesson06;

import java.util.Map;
import java.util.TreeMap;

public class BeenRepresenter {
    public static <T extends Object> void representer(T object) {
        Map<String,T> map = new TreeMap<>();
        map = ReflectionInterface.getObjectFieldsValues(object);
        map.forEach((String key,T value) ->  {
            System.out.println(String.format("Propery is %40s;\t value = %s", key, value));
        });
    }   
}
