package lesson05;

import  lesson05.source.URLSourceProvider;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.net.UnknownHostException;

/**
 * Provides utilities for translating texts to russian language.<br/>
 * Uses Yandex Translate API, more information at <a href="http://api.yandex.ru/translate/">http://api.yandex.ru/translate/</a><br/>
 * Depends on {@link URLSourceProvider} for accessing Yandex Translator API service
 */
public class Translator {
    private URLSourceProvider urlSourceProvider;
    /**
     * Yandex Translate API key could be obtained at <a href="http://api.yandex.ru/key/form.xml?service=trnsl">http://api.yandex.ru/key/form.xml?service=trnsl</a>
     * to do that you have to be authorized.
     */
    private static final String YANDEX_API_KEY = "trnsl.1.1.20141121T002123Z.9fbe849b3354c937.a6437a6eec4b5c8fa25228f13f04636d9ddfe59a";
    private static final String TRANSLATION_DIRECTION = "en-ru";

    public Translator(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
    }

    /**
     * Translates text to russian language
     * @param original text to translate
     * @return translated text
     * @throws IOException
     */
    public String translate(String original) throws IOException {
        int responseCode = 0;
        HttpURLConnection httpConnection = null;
        try {
            URL url = new URL(prepareURL(original));
            httpConnection = (HttpURLConnection) url.openConnection();
            responseCode = httpConnection.getResponseCode();
        } catch (UnknownHostException unknownHostException) {
            System.out.println(unknownHostException.getMessage());
        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println(fileNotFoundException.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
 
        if (responseCode == 200) {
//            String loadString = null;
//            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));
//            StringBuilder strBuilder = new StringBuilder();
//            while ((loadString = bufferedReader.readLine()) != null) {
//                strBuilder.append(loadString).append('\n');
//            }
//            return parseContent(String.valueOf(strBuilder));
           return parseContent(String.valueOf(this.urlSourceProvider.load(prepareURL(original))));
        }
        return "Can\'t translate: answer from Yandex Translate Server";
    }

    /**
     * Prepares URL to invoke Yandex Translate API service for specified text
     * @param text to translate
     * @return url for translation specified text
     */
    private String prepareURL(String text) throws UnsupportedEncodingException {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" + YANDEX_API_KEY + "&text=" + encodeText(text) + "&lang=" + TRANSLATION_DIRECTION;
    }

    /**
     * Parses content returned by Yandex Translate API service. Removes all tags and system texts. Keeps only translated text.
     * @param content that was received from Yandex Translate API by invoking prepared URL
     * @return translated text
     */
    private String parseContent(String content) throws UnsupportedEncodingException {
        int startText = content.indexOf("<text>");
        int endText = content.indexOf("</text>");
        return content.substring((startText  + "<text>".length()), endText );
    }

    /**
     * Encodes text that need to be translated to put it as URL parameter
     * @param text to be translated
     * @return encoded text
     */
    private String encodeText(String text) throws UnsupportedEncodingException {
        return URLEncoder.encode(text, "UTF-8"); 
    }
}
