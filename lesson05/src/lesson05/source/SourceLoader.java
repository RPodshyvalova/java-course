package lesson05.source;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader() {
        sourceProviders.add(new URLSourceProvider());
        sourceProviders.add(new FileSourceProvider());
    }

    public String loadSource(String pathToSource) throws IOException {
        String sourcetString = "";
        
        if (pathToSource.contains("http") && sourceProviders.get(0).isAllowed(pathToSource)) {
            sourcetString = sourceProviders.get(0).load(pathToSource); 
        } else {
            if (pathToSource.contains(File.separator) && sourceProviders.get(1).isAllowed(pathToSource)) {
                sourcetString = sourceProviders.get(1).load(pathToSource); 
            }
        }
        return sourcetString; 
    }
  
}
