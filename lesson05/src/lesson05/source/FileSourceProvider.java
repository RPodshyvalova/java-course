package lesson05.source;

import java.io.*;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {
   @Override
    public boolean isAllowed(String pathToSource) {
        try {
            File f = new File(pathToSource);
            Boolean flag = f.exists();
            if(f.isFile() && flag) return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return false; 
    }

    @Override
    public String load(String pathToSource) throws IOException {
        String loadString;
        StringBuilder text = new StringBuilder();
               
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(pathToSource)))) {
            while ((loadString = bufferedReader.readLine()) != null){
                text.append(loadString).append('\n');
            }
        }
       return String.valueOf(text);
    }
}
