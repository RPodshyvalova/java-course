package lesson05.source;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        int responseCode = 0;
        try {
            URL url = new URL(pathToSource);
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            responseCode = httpConnection.getResponseCode();
        } catch (UnknownHostException unknownHostException) {
            System.out.println(unknownHostException.getMessage());
        } catch (FileNotFoundException fileNotFoundException) {
            System.out.println(fileNotFoundException.getMessage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return responseCode == 200;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        String loadString;
        URL url = new URL(pathToSource);
        StringBuilder text = new StringBuilder();
              
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            while ((loadString = bufferedReader.readLine()) != null){
                text.append(loadString).append('\n');
            }
        }
        return String.valueOf(text);
    }
}
