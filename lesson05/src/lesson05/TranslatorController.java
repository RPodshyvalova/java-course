package lesson05;

import lesson05.source.SourceLoader;
import lesson05.source.URLSourceProvider;
import java.io.IOException;
import java.util.Scanner;


public class TranslatorController {

    public static void main(String[] args) throws IOException {
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator( new URLSourceProvider());

//        System.out.println("Hi! Please, give me what you wanna translate:");
//        Scanner scanner = new Scanner(System.in);
//        String command = scanner.next();
//        Test date       
        String command="https://dl.dropboxusercontent.com/u/14434019/en.txt";
//        String command="D:\taskGeekHub05.txt";
//        while (!"exit".equals(command)) {
                String source = sourceLoader.loadSource(command);
                if (source.length()>0) {
                    String translation = translator.translate(source);
                    System.out.println("------------------------------------------------------------------------------------");   
                    System.out.println("***** The original document:\n\n" + source);
                    System.out.println("***** Translated document:\n\n" + translation);
                    System.out.println("------------------------------------------------------------------------------------");   
//                    System.out.println("More translation? or type the word exit for exit: ");
                } else  {
                    System.out.println("Wrong path. Please try again or type the word exit for exit:");
                }
//                command = scanner.next();
//        }
    }
}
