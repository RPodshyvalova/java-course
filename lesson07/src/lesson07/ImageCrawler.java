package lesson07;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster. To start download images you should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {
    private Set<URL> links = new HashSet<>();
    private Set<URL> visitedLinks = new HashSet<>();
    private Set<URL> set = new HashSet<>();
    public static final int NUMBER_OF_THREADS = 10;
    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
    }

    /**
     * Call this method to start download images from specified URL.
     * @param urlToPage
     * @throws IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        //implement me
        Page page = new Page(new URL(urlToPage));
        Set<URL> imageLinks = new HashSet<>();
        imageLinks.addAll(page.getImageLinks());
        
        imageLinks.forEach( (url) -> {
            if (isImageURL(url)) { 
                Runnable runner =  new ImageTask(url, folder);
                executorService.execute(runner);
            }
        });      

        visitedLinks.add(new URL(urlToPage));
        links.addAll(page.getLinks());
        links.removeAll(visitedLinks);
        if (links.size() > 0) {
            links.stream().forEach((URL url) -> {
                try {
                    System.out.println("current url is: " + url.toString());
                    downloadImages(url.toString());
                } catch (IOException e) {
                    Logger.getLogger(ImageCrawler.class.getName()).log(Level.SEVERE, null, e);
                }
            }); 
        
        }
    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    //detects is current url is an image. Checking for popular extensions should be enough
    private boolean isImageURL(URL url) {
        //implement me
        Pattern pattern = Pattern.compile("([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)");
        Matcher matcher = pattern.matcher(url.toString());
        return matcher.matches();
    }
}
