package lesson07;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {
        //implement me
        ByteArrayOutputStream array = new ByteArrayOutputStream();
        int n;
        try (InputStream inputStream = url.openStream ()) {
            byte[] buffer = new byte[20480]; 
            while ((n = inputStream.read(buffer)) > 0 ) {
                array.write(buffer, 0, n);
            }
        } catch (IOException e) {   
            e.getMessage();
        } 
        return array.toByteArray();
    }
}
