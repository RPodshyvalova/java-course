package lesson07;

import java.util.concurrent.TimeUnit;


public class Test {

    public static void main(String[] args) throws InterruptedException {
        final GeneralSource generalSource = new GeneralSource();

        new Thread(new Runnable() {
            @Override
            public void run() {
                generalSource.synchornized1();
            }
        }).start();

        TimeUnit.MILLISECONDS.sleep(100);

        new Thread(new Runnable() {
            @Override
            public void run() {
                generalSource.nonsynchronizedChangeState();
                generalSource.nonsynchronizedNoNChangeState();
                generalSource.synchronized2();
            }
        }).start();

    }
}

class GeneralSource{
    private int c = 0;
    public synchronized void synchornized1(){
        try {
            System.out.println("synchronized1: c="+c+" - object "+this +" locked");
            TimeUnit.SECONDS.sleep(10);
            System.out.println("synchronized1: c="+c+" - object "+this +" unlocked");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public synchronized void synchronized2(){
        System.out.println("synchronized2: c="+c+" - object "+this +" locked");
        c += 1;
        System.out.println("synchronized2: c="+c+" - object "+this +" unlocked");
    }

    public void nonsynchronizedChangeState(){
        c += 1;
        System.out.println("nonsynchronizedChangedState: c="+c+ " work with object "+this );
    }

    public void nonsynchronizedNoNChangeState(){
        System.out.println("nonsynchronizedNonChangedState: c="+c+ " work with object "+this );
    }
}

