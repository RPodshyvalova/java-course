/* global websocket */

//var wsUri = "ws://localhost:8080/psi/endpoint"; 
var wsUri = "ws://" + document.location.host + "/psi/endpoint";

websocket = new WebSocket(wsUri); 
websocket.onopen = function(e) {
    $('#output').append("<div>Let\'s talk!</div>"); 
};
websocket.onmessage = function(e) {
    var message = JSON.parse(e.data);
    if (message.message) {
        var currentUsersFraseDiv = document.createElement("div");
        var userNameSpan = document.createElement("span");
        var currentUserMessageSpan = document.createElement("span");
        $(userNameSpan).text(message.login + " : ").appendTo($(currentUsersFraseDiv)); 
        $(currentUserMessageSpan).text(message.message).appendTo($(currentUsersFraseDiv)); 
        $(currentUsersFraseDiv).prependTo("#chatHistory");
    }
};

websocket.onerror = function(e){$('#output').append("<div>Error  - " + e.data + "</div>");}; 
websocket.onclose = function(e){$('#output').append("<div>Connection Closed</div>");}; 

$('#message').keyup(function(e){
    if (e.keyCode === 13) {
        sendMessage();
    }
});

$("#sendButton").click(function() {
    sendMessage();
});
    
function sendMessage() {
    if ($("#message").val()) {
        websocket.send($("#message").val());
        $("#message").val("");
    }
  }
  
