CREATE USER root@localhost identified BY 'password';
GRANT usage ON *.* TO root@localhost identified BY 'password';
CREATE DATABASE IF NOT EXISTS psi CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL privileges ON psi.* TO root@localhost;

use psi;


CREATE TABLE users (
     
userId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
     
login VARCHAR(30) NOT NULL,
     
passwordValue VARCHAR(30) NOT NULL,
     
firstname VARCHAR(30),
     
lastname  VARCHAR(30),
     
phone VARCHAR(15),
     
email     VARCHAR(30),
     
created   TIMESTAMP DEFAULT NOW(),
     
roles VARCHAR(30),

active VARCHAR(1)
  
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_general_ci;

CREATE TABLE schedule (
      
scheduleId INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
      
task VARCHAR(2000) NOT NULL,
      
datecreate TIMESTAMP DEFAULT NOW(),
      
dateremind TIMESTAMP null,
      
userId  INT,
      
FOREIGN KEY (userId) REFERENCES users(userId) ON UPDATE CASCADE ON DELETE RESTRICT
  
) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8 COLLATE = utf8_general_ci;



use psi;
insert into users values(null, "doctor", "doctor", "", "", "", "", null, "ROLE_ADMIN", "Y");
