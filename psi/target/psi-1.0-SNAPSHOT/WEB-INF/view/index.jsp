<%@page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@page session="true"%>

<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<c:url value="/resources/style.css" />" type="text/css" rel="stylesheet" />
    </head>
<!--    body   -->
    <body>
        <header>
             <div id="login">
                <security:authorize access="isAuthenticated()">
                    <div id="logout">
                        <a href="<c:url value="/logout" />"><p style="color: #2697d1;">out</p></a>
                    </div>
                </security:authorize>
                <security:authorize access="isAnonymous()">
                    <form method="POST" action="/psi/j_spring_security_check">
                        <input type="submit" value="" id="buttonLogin" />
                        <input type="text" name="j_password" placeholder="password" class="loginForm" />
                        <input type="text" name="j_username"  placeholder="login" class="loginForm" />
                    </form>
                </security:authorize>
            </div>
            <div id="logo">
                <h1 class="title">Dr. Nobodys<br>psychological<br>practice</h1>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="#" style="border: solid 1px #ffffff; padding:2px;">Home</a></li>
                    <li><a href="<c:url value="/registration/create" />">Registration</a></li>
                    <security:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/schedule/byDay" />">Schedule</a></li>
                        <li><a href="<c:url value="/chat/create" />">Chart</a></li>
                    </security:authorize>
                    <security:authorize access="isAnonymous()">
                        <li><a href="<c:url value="/signin-failure" />">Schedule</a></li>
                        <li><a href="<c:url value="/signin-failure" />">Chart</a></li>
                    </security:authorize>
                    <li><a href="#">Contacts</a></li>
                </ul>
            </div>
       
        </header>
        <div id="main"> 
            <a href="<c:url value="/registration/create" />">
                <div id="registrationBox">
                    <h1>step_1. REGISTRATION</h1>
                    <img src="<c:url value="/resources/images/registration.png" />">
                    <p class="titleTxt">Simple registration. All your information only for internal use. Full confidentiality.</p>
                </div>
            </a>
            <security:authorize access="isAuthenticated()">
                <a href="<c:url value="/schedule/byDay" />">
                    <div id="scheduleBox">
                        <h1>step_2. SCHEDULE</h1>
                        <img src="<c:url value="/resources/images/schedule.png" />">
                        <p class="titleTxt">Register in consultation in any for you convenient time.</p>
                    </div>
                </a>
                <a href="<c:url value="/chat/create" />">
                    <div id="chatBox">
                        <h1>step_3. CHAT</h1>
                        <img src="<c:url value="/resources/images/message.png" />">
                        <p class="titleTxt">Go to text chat with Dr. Nobody.</p>
                    </div>
                </a>
            </security:authorize>
            <security:authorize access="isAnonymous()">
                <a href="<c:url value="/signin-failure" />">
                    <div id="scheduleBox">
                        <h1>step_2. SCHEDULE</h1>
                        <img src="<c:url value="/resources/images/schedule.png" />">
                        <p class="titleTxt">Register in consultation in any for you convenient time.</p>
                    </div>
                </a>
                <a href="<c:url value="/signin-failure" />">
                    <div id="chatBox">
                        <h1>step_3. CHAT</h1>
                        <img src="<c:url value="/resources/images/message.png" />">
                        <p class="titleTxt">Go to text chat with Dr. Nobody.</p>
                    </div>
                </a>
            </security:authorize>        
                    
        </div>
        <footer>
            <p class="fraseTxt">Copyright 2015 RPodshyvalova</p>
        </footer>    
    </body>
</html>
