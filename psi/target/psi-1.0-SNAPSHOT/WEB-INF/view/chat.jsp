<%@ page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <link href="<c:url value="/resources/style.css" />" type="text/css" rel="stylesheet" />
    </head>
<!--    body   -->
    <body>
        <header>
            <security:authorize access="isAnonymous()">
                <div id="login">
                    <form method="POST" action="/psi/j_spring_security_check">
                        <input type="submit" value="" id="buttonLogin" />
                        <input type="text" name="j_password" placeholder="password" class="loginForm" />
                        <input type="text" name="j_username"  placeholder="login" class="loginForm" />
                    </form>
                </div>
            </security:authorize>
            <div id="logo">
                <h1 class="title">Dr. Nobodys<br>psychological<br>practice</h1>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="<c:url value="/" />">Home</a></li>
                    <li><a href="<c:url value="/registration/create" />">Registration</a></li>
                    <li><a href="<c:url value="/schedule/byDay" />">Schedule</a></li>
                    <security:authorize access="isAuthenticated()">
                        <li><a href="#" style="border: solid 1px #ffffff; padding:2px;">Chart</a></li>
                    </security:authorize>
                    <li><a href="#">Contacts</a></li>
                </ul>
            </div>
        </header>
        <div id="main"> 
            <div id="output"></div>
            <security:authorize access="isAuthenticated()">
                <c:set var="currentUser">
                    <security:authentication property = "principal.username"/>
                </c:set>
                <c:out value="${currentUser}" />
                <c:if test="${! empty message}">
                    <p class="error">
                        your time is <c:out value="${message}" />
                    </p>
                </c:if>
            </security:authorize>
            <div  style="position: relative; float:left; width: 98%;">
                <input type="text" id="message" placeholder="Type your message" style="position:relative; float:right; width:100%; height:40px; margin-top:20px; background-color: #ffffc6; border:none;" />
                <button id="sendButton"  class="simpleButton" style="position:relative; float:right; margin-right: 50px;">Talk</button>
            </div>
            <div id="chatHistory"  style="position: relative; float:left; width: 98%; height: 60%; padding-top: 2%; color:black; background-color: #e6f1fc; border: none; overflow:auto;"></div> 
 
            <script type="text/javascript" src="<c:url value="/resources/jquery-2.1.3.min.js" />"></script>
            <script type="text/javascript" src="<c:url value="/resources/websocket.js" />"></script>
        </div>
    </body>
</html>

