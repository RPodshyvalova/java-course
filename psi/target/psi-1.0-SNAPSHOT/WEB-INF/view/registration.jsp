<%@page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sform" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@page session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<c:url value="/resources/style.css" />" type="text/css" rel="stylesheet" />
    </head>
<!--    body   -->    
    <body>
        <header>
            <security:authorize access="isAnonymous()">
                <div id="login">
                    <form method="POST" action="/psi/j_spring_security_check">
                        <input type="submit" value="" id="buttonLogin" />
                        <input type="text" name="j_password" placeholder="password" class="loginForm" />
                        <input type="text" name="j_username"  placeholder="login" class="loginForm" />
                    </form>
                </div>
            </security:authorize>
            <div id="logo">
                <h1 class="title">Dr. Nobodys<br>psychological<br>practice</h1>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="<c:url value="/" />">Home</a></li>
                    <li><a href="#" style="border: solid 1px #ffffff; padding:2px;">Registration</a></li>
                    <security:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/schedule/byDay" />">Schedule</a></li>
                        <li><a href="<c:url value="/chat/create" />">Chart</a></li>
                    </security:authorize>
                    <security:authorize access="isAnonymous()">
                        <li><a href="<c:url value="/signin-failure" />">Schedule</a></li>
                        <li><a href="<c:url value="/signin-failure" />">Chart</a></li>
                    </security:authorize>
                    <li><a href="#">Contacts</a></li>
                </ul>
            </div>
        </header>
       
        <div id="main"> 
            <sform:form method="POST" modelAttribute="user" style="position: relative; top:10px; width:98%; color : #2697d1; font-weight: bold;">   
                <fieldset style="border: solid 1px #2697d1; ">  
                    <legend>Registration form:</legend>
                    <div class="formLine">
                        <label for="loginUser">Login<sup>*</sup>:</label>
                        <sform:input path="login" id="loginUser" />
                        <sform:errors path="login" class="error" />
                    </div>
                    <div class="formLine">
                        <label for="passwordUser">Password<sup>*</sup>:</label>
                        <sform:password path="passwordValue" showPassword="true" style="color: #000000;" id="passwordUser" /> 
                        <sform:errors path="passwordValue" class="error" />
                    </div>    
                    <div class="formLine">
                        <label for="firstnameUser">First name:</label>
                        <sform:input path="firstname" id="firstnameUser" />
                    </div>
                    <div class="formLine">
                        <label for="lastnameUser">Last name:</label>
                        <sform:input path="lastname" id="lastnameUser" />
                    </div>
                    <div class="formLine">
                        <label for="phoneUser">Phone:</label>
                        <sform:input path="phone" id="phoneUser"/>
                    </div>
                    <div class="formLine">
                        <label for="emailUser">Email<sup>*</sup>:</label>
                        <sform:input path="email" id="emailUser" />
                        <sform:errors path="email" class="error" />
                    </div>
                    <div class="formLine" style="height:30px;">
                        <input type="submit" name="submit" value="Register" style="margin-top:1px; margin-left:15%;" />
                        <input type="reset" name="reset" value="Reset" style="margin-top:1px; margin-left:30px;" />
                    </div>
                    <div  class="fraseTxt" style="margin-top:20px;  margin-left:5%; font-weight: normal;">
                        <p>* required fields</p>
                    </div>
                </fieldset>
            </sform:form>

            <div style="margin-top:10px;  margin-left:2%">
                <c:if test="${! empty message}">
                    <p class="error">
                        <c:out value="${message}" />
                    </p>
                </c:if>
            </div>
        </div>
        
        <footer>
            
        </footer>
    </body>
</html>
