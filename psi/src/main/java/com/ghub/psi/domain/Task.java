package com.ghub.psi.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "SCHEDULE")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="scheduleId")
    private Integer taskId; 
    
    @Column
    private String task;
    
//    @Column
//    private Date datecreate;
    
    @Column(name="dateremind")
    private Date dateRemind;
    
    @ManyToOne
    @JoinColumn(name="userId")
    private User user;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Date getDateRemind() {
        return dateRemind;
    }

    public void setDateRemind(Date dateRemind) {
        this.dateRemind = dateRemind;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
