package com.ghub.psi.schedule.models.renderer;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class DailyModelRenderer implements ITableModelRenderer {
    private String model[][];
    
    public DailyModelRenderer() {
        model = new String[24][2]; 
    }

    @Override
    public void colorizeTableScheduleCells() {
        for (int row=0; row<model.length; row++) {
            for (int col=0; col<model[row].length; col++) {
                model[row][col] = (col == 1 && row < 9 || col== 1 && row > 18 ? "#ffffc6" : null);
                if (col==0) {
                    model[row][col] = "#e4ebf2";
                }
                if (col==1) {
                    if(row < 9 || row > 18) { 
                        model[row][col] = "#ffffde";
                    } else {
                        model[row][col] = "#ffffc6";
                    }        
                } else {
                    model[row][col] = "#e4eef7";
                }
            }
        }
    }
        
    @Override
    public String[][] getColorizedModel() {
        colorizeTableScheduleCells();
        return model;
    }
}
