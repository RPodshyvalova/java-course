package com.ghub.psi.schedule.models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JFormattedTextField.AbstractFormatter;


public class DateLib extends AbstractFormatter{
    public static String longFormatDate = "dd.MM.yyyy HH:mm:ss";
    public static String shortFormatDate = "dd.MM.yyyy";
    public static String ukrFormatDate = "EEEE, d MMMM yyyy �.";
    public static SimpleDateFormat formatterDate;
    public static String mySqlFormatDate = "%d.%m.%Y %H:%i:%s";
    private static Calendar calendar;
    
    
    public DateLib() {
        calendar = Calendar.getInstance(); 
        formatterDate = new SimpleDateFormat(longFormatDate);
    }
    
    public DateLib(String formatStr) {
        calendar = Calendar.getInstance(); 
        formatterDate = new SimpleDateFormat(formatStr);
    }
    
    public DateLib(Date date, String formatStr) {
        calendar.setTime(date); 
        formatterDate = new SimpleDateFormat(formatStr);
    }
    
    public Object changeDateFormat(Object value) throws ParseException {
        return stringToValue(valueToString(value));
    }
    
    @Override
    public  Object stringToValue(String text) throws ParseException {
        return formatterDate.parseObject(text);
    }

    @Override
    public String valueToString(Object value) throws ParseException {
        return formatterDate.format(value);
    }

    public Date stringToDate(String text) throws ParseException {
        return (Date)stringToValue(text);
    }
    
    public static Date addDays(Date date, int daysToAdd) {
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, daysToAdd);
        return calendar.getTime();
    }
    
    public static Date addMonths(Date date , int monthsToAdd) {
        calendar.setTime(date);
        calendar.add(Calendar.MONTH, monthsToAdd);
        return calendar.getTime();
    }
    
    public static Date substDays(Date date, int daysToAdd) {
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, -daysToAdd);
        return calendar.getTime();
    }
    
    public static Date getFirstDayOfMounth(Date date) {
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }
    
    public static Date getLastDayOfMonth(Date date) {
        calendar.setTime(date);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        return calendar.getTime();
    }
    
    public static Date getFirstDayOfYear(Date date) {
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }
    
    public static Date getLastDayofYear(Date date) {
        calendar.setTime(getFirstDayOfYear(date));
        calendar.add(Calendar.YEAR, 1);
        return calendar.getTime();
    }
    
    public static int getCountDaysOfMonth(Date date){
        calendar.setTime(date);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
    
}
