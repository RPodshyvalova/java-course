package com.ghub.psi.schedule.models;

import com.ghub.psi.domain.Task;
import com.ghub.psi.domain.User;
import com.ghub.psi.service.ITaskService;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class DailyModel implements ITableModel {
    @Autowired
    private ITaskService taskService;    
    private String model[][];
    private Date startPeriod;
    private Date endPeriod;

    public DailyModel(){
        model = new String[24][2];
    }

    @Override
    public void fillModelByData() {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Task> list = taskService.listTaskByPeriod(startPeriod, endPeriod); 
        Map<Date, String> data = filterListTasks(user, list);
        for (Map.Entry entry : data.entrySet()) {
            Date date = (Date)entry.getKey();
            String value = entry.getValue() != null ? entry.getValue().toString().trim() : "";
            int row = date.getHours();
            try {
                model[row][0] = new DateLib().valueToString(date.getHours());
            } catch (ParseException ex) {
                Logger.getLogger(DailyModel.class.getName()).log(Level.SEVERE, null, ex);
            }
            model[row][1] = model[row][1] == null ? new String() : model[row][1];
            model[row][1] += date.getHours() + ":" + date.getMinutes() + "  " 
                    + value + "; ";
        } 
        
        for (int i=0; i<model.length; i++) {
            model[i][0] = String.valueOf(i);
        }
    }
    
    @Override
    public void setPeriod(Date startPeriod, Date endPeriod) {
        this.startPeriod = startPeriod;
        this.endPeriod = endPeriod;
    }
    
    @Override
    public String[][] getModel(Date startPeriod, Date endPeriod) {
        setPeriod(startPeriod, endPeriod);
        fillModelByData();
        return model;
    }

}
