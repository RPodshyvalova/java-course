package com.ghub.psi.schedule.models;

import com.ghub.psi.domain.Task;
import com.ghub.psi.domain.User;
import com.ghub.psi.service.ITaskService;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class MonthlyModel implements ITableModel {
    @Autowired
    private ITaskService taskService;  
    private String model[][]; 
    private Date startPeriod;
    private Date endPeriod;
    
    public MonthlyModel(){
        model = new String[6][7];
        startPeriod = null;
        endPeriod = null;
    }
    
    @Override
    public void fillModelByData() {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Task> list = taskService.listTaskByPeriod(startPeriod, endPeriod); 
        Map<Date, String> data = filterListTasks(user, list);
        if(!data.isEmpty()) {
            Date dateFirstTask =(Date)data.keySet().toArray()[0];
            int shift = DateLib.getFirstDayOfMounth(dateFirstTask).getDay();
            shift = (shift == 0 ? 5: shift-1);

            for (Map.Entry entry : data.entrySet()) {
                Date date = (Date)entry.getKey();
                String value = entry.getValue()!=null ? entry.getValue().toString().trim() : "";
                int day = date.getDate();
                int number = date.getDay();
                int row = (day+shift) / 7;  
                int column = number == 0 ? 6 : number-1;

                model[row][column] = (model[row][column] == null ? new String() : model[row][column]);  // delete null values in cells
                model[row][column] += date.getHours() + ":" + date.getMinutes() + "  " 
                        + value + "; ";
            }
        }
    }
    
    @Override
    public void setPeriod(Date startPeriod, Date endPeriod) {
        this.startPeriod = startPeriod;
        this.endPeriod = endPeriod;
    }
    
    @Override
    public String[][] getModel(Date startPeriod, Date endPeriod) {
        setPeriod(startPeriod, endPeriod);
        fillModelByData();
        return model;
    }
}
