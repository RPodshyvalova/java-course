package com.ghub.psi.schedule.models.renderer;

public interface ITableModelRenderer {
    
    abstract public void colorizeTableScheduleCells(); 
    
    abstract public String[][] getColorizedModel();
}
