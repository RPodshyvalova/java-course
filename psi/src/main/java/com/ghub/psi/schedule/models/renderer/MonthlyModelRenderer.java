package com.ghub.psi.schedule.models.renderer;

import com.ghub.psi.schedule.models.DateLib;
import java.util.Date;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(value="prototype")
public class MonthlyModelRenderer implements ITableModelRenderer {
    private String model[][];
    private String numbers[][];
    private Date date;
    
    public MonthlyModelRenderer() {
        model = new String[6][7];
        numbers = new String[6][7];
        date = null;
    }
   
    @Override
    public void colorizeTableScheduleCells() {
        int shift;
        int rowLastDay;
        int columnLastDay;
        shift = date.getDay();
        shift = (shift == 0 ? 5 : shift - 1);
        int daysCount = DateLib.getCountDaysOfMonth(this.date) + 1;
        rowLastDay = (daysCount + shift) / 7;  
        columnLastDay = (daysCount + shift) % 7;
        int number = 1;
        for (int row=0; row<model.length; row++) {
            for (int col=0; col<model[row].length; col++) {
                if (row == 5 && rowLastDay != 5) { 
                    model[row][col] = "#e4ebf2";
                } else {
                    if (row == 0 && col <= shift || row == rowLastDay && col >= columnLastDay) {
                        model[row][col] = "#e4ebf2";
                    } else {
                        model[row][col] = "#ffffde";
                        numbers[row][col] = String.valueOf(number);
                        number++;
                    } 
                }
      
            }
        }
    }

    public void setDate(Date date) {
        this.date = date;
    }
     
    @Override
    public String[][] getColorizedModel() {
        colorizeTableScheduleCells();
        return model;
    }
    
    public String[][] getNumberDaysOfMonthModel() {
        return numbers;
    }
}
