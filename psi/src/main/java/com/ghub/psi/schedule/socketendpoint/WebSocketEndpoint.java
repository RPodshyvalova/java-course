package com.ghub.psi.schedule.socketendpoint;

import com.ghub.psi.domain.User;
import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/endpoint")
public class WebSocketEndpoint {
    private static Set<Session> connections = java.util.Collections.synchronizedSet(new HashSet<Session>());
    private static Map<Session, String> loginsList = new ConcurrentHashMap<>();
    private Session currentSession;

    @OnOpen
    public void onOpen (Session session) {
        connections.add(session);
        this.currentSession = session;
    }

    @OnMessage
    public void onMessage (String message) {

        String name =  currentSession.getUserPrincipal().getName(); 
        if (!loginsList.containsKey(currentSession)) {
            loginsList.put(currentSession,  name);
        }
        String messageToSend = "{\"login\":\"" + loginsList.get(currentSession)
            + "\", \"message\":\"" + message.replace("\"", "\\\"") +"\"}";
        for (Session session : connections) {
            try {
                session.getBasicRemote().sendText(messageToSend);
            } catch (IOException ex) {
                Logger.getLogger(WebSocketEndpoint.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    } 

    @OnClose
    public void onClose (Session session, CloseReason reason) {
        String login = loginsList.get(session);
        connections.remove(session);
        loginsList.remove(session);
    }

    @OnError
    public void onError (Session session, Throwable throwable) {
        String login = loginsList.get(session);
        connections.remove(session);
        loginsList.remove(session);
    }
}
   
