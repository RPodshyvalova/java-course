package com.ghub.psi.schedule.models;

import com.ghub.psi.domain.Task;
import com.ghub.psi.domain.User;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface ITableModel {
    
    abstract public void fillModelByData();
    
    abstract public void setPeriod(Date startPeriod, Date endPeriod);
    
    abstract public String[][] getModel(Date startPeriod, Date endPeriod);
    
    default public Map<Date, String> filterListTasks(User user, List<Task> list) {
        Map<Date, String> data = new  HashMap<>();
        if (!list.isEmpty()) {
            for (Task task : list) {
                if (user.getRoles().equals("ROLE_ADMIN")){
                    data.put(task.getDateRemind(), task.getTask());
                } else {
                    if (user.getRoles().equals("ROLE_USER") && !task.getUser().getLogin().equals(user.getLogin())) {
                        data.put(task.getDateRemind(), "it\'s reserved");    
                    } else {
                        if (user.getRoles().equals("ROLE_USER") && task.getUser().getLogin().equals(user.getLogin())){
                            data.put(task.getDateRemind(), task.getUser().getLogin() + " - " + task.getTask());   
                        }
                    }
                }
            }
        }
        return data;
    }
}
