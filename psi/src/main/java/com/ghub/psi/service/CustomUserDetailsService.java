package com.ghub.psi.service;

import com.ghub.psi.dao.UserDAOImpl;
import com.ghub.psi.domain.User;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserDAOImpl userDAOImpl;
    private HashMap<String, User> users; 
    private List<User> list;
    
    private User getUser(String username) throws UsernameNotFoundException{
        users = new HashMap<>();
        list = userDAOImpl.listUser();
        if (!list.isEmpty()) {
            for (User user : list) {
                users.put(user.getLogin(), user);
            }
        }
	if (!users.containsKey(username )) {
            throw new UsernameNotFoundException(username + " not found");
	}
	return users.get(username);		
    }   
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        return (UserDetails)getUser(username);
    }
    
}
