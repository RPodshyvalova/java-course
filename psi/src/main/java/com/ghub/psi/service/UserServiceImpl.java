package com.ghub.psi.service;

import com.ghub.psi.dao.UserDAOImpl;
import java.util.List;
import com.ghub.psi.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
@Service("iUserService")
public class UserServiceImpl implements IUserService {
    @Autowired
    private UserDAOImpl userDAOImpl;
    
    @Override
    public void addUser(User user) {
        userDAOImpl.addUser(user);
    }

    @Override
    public List<User> listUser() {
        return userDAOImpl.listUser();
    }

    @Override
    public void removeUser(Integer id) {
         userDAOImpl.removeUser(id);
    }
}
