package com.ghub.psi.service;

import com.ghub.psi.dao.TaskDAOImpl;
import com.ghub.psi.domain.Task;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
@Service("iTaskService")
public class TaskServiceImpl implements ITaskService{
    @Autowired
    private TaskDAOImpl taskDAOImpl;
    
    @Override
    public void addTask(Task task) {
        taskDAOImpl.addTask(task);
    }
    
    @Override
    public List<Task> listTask() {
        return taskDAOImpl.listTask();
    }
    
    @Override
    public void removeTask(Integer id) {
        taskDAOImpl.removeTask(id); 
    }

    @Override
    public List<Task> listTaskByPeriod(Date startPeriod, Date endPeriod) {
        return taskDAOImpl.listTaskByPeriod(startPeriod, endPeriod);
    }
}
