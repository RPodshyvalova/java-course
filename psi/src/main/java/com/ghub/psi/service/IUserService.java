package com.ghub.psi.service;

import com.ghub.psi.domain.User;
import java.util.List;

public interface IUserService {
    
    public void addUser(User user);
    
    public List<User> listUser();
    
    public void removeUser(Integer id);
}
