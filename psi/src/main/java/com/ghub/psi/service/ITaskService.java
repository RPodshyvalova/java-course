package com.ghub.psi.service;

import com.ghub.psi.domain.Task;
import java.util.Date;
import java.util.List;

public interface ITaskService {
    
    public void addTask(Task task);
    
    public List<Task> listTask();
    
    public List<Task> listTaskByPeriod(Date startPeriod, Date endPeriod);
    
    public void removeTask(Integer id);       
}
