package com.ghub.psi.validation;

import com.ghub.psi.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";    

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login", "valid.name");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "passwordValue", "valid.passwordValue");
        if (!user.getEmail().matches(EMAIL_PATTERN)) {
            errors.rejectValue("email","valid.email");
	}
    }
}
