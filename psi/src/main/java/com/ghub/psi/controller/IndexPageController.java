package com.ghub.psi.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class IndexPageController {

    @RequestMapping({"/","/index"})
    public String home() {
        return "index";
    }   
    
    @RequestMapping(value = "/signin-failure", method = RequestMethod.GET)
    public String signinFailure() {
	return "signin_failure";
    }
}
