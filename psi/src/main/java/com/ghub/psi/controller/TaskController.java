package com.ghub.psi.controller;

import com.ghub.psi.domain.Task;
import com.ghub.psi.domain.User;
import com.ghub.psi.schedule.models.DateLib;
import com.ghub.psi.schedule.models.ITableModel;
import com.ghub.psi.schedule.models.renderer.ITableModelRenderer;
import com.ghub.psi.schedule.models.renderer.MonthlyModelRenderer;
import com.ghub.psi.service.ITaskService;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/schedule")
@Controller
@Scope(value="prototype")
public class TaskController {
    @Autowired
    private ITableModel dailyModel;    
    @Autowired
    private ITableModelRenderer dailyModelRenderer;    
    @Autowired
    private ITableModel  weeklyModel;   
    @Autowired
    private ITableModelRenderer weeklyModelRenderer;   
    @Autowired
    private ITableModel monthlyModel;    
    @Autowired
    private MonthlyModelRenderer monthlyModelRenderer;   
    private Date startPeriod; 
    private Date endPeriod;
    private Date currentDay;
    private String[][] dataModel;
    private String[][] dataModelRenderer;
    @Autowired
    private ITaskService taskService;
    
    @RequestMapping(value={"/create"}, method=RequestMethod.POST)
    public String createTask(HttpServletRequest request, Model model) throws ParseException {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Task task = new Task();
        task.setTask("new task");
        task.setDateRemind(new DateLib().stringToDate(request.getParameter("date")));
        task.setUser(user);
        task.setTask(request.getParameter("task"));
        taskService.addTask(task);
        model.addAttribute("createTask", "Your request is implemented successfuly");
        return "redirect:/schedule/byDay";
    }
    
//    @RequestMapping(value="/byMonth/{date}", method=RequestMethod.GET)
//    public String printMonthlySchedule(@PathVariable("date") Date date, Model model) throws ParseException {
    @RequestMapping(value={"/byDay"}, method=RequestMethod.GET)
    public String printDailySchedule(Model model) throws ParseException {
        String[] headerScheduleTable = {"Hours", "Tasks"};
        model.addAttribute("headerScheduleTable", headerScheduleTable);
        currentDay = (Date)new DateLib().changeDateFormat(new Date());
        currentDay.setHours(0);
        currentDay.setMinutes(0);
        currentDay.setSeconds(0);
        startPeriod = currentDay; 
        endPeriod = DateLib.addDays(currentDay, 1);

        dataModel = dailyModel.getModel(startPeriod, endPeriod);
        model.addAttribute("scheduleDaily", dataModel);
     
        dataModelRenderer = dailyModelRenderer.getColorizedModel();
        model.addAttribute("scheduleDailyRenderer",  dataModelRenderer);
       
        model.addAttribute("value",  "Daily view by " + currentDay);
       
        return "schedule";
    }
    

    @RequestMapping(value="/byWeek", method=RequestMethod.GET)
    public String printWeeklySchedule(Model model) throws ParseException {
        String[] headerScheduleTable = {"Hours", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        model.addAttribute("headerScheduleTable", headerScheduleTable);

        currentDay = (Date)new DateLib().changeDateFormat(new Date());
        startPeriod = currentDay.getDay()==0 ? DateLib.substDays(currentDay, 7-1) : 
            DateLib.substDays(currentDay, currentDay.getDay()-1);
        endPeriod  = DateLib.addDays(startPeriod, 7-1);
        dataModel = weeklyModel.getModel(startPeriod, endPeriod);
        model.addAttribute("scheduleWeekly", dataModel);
       
        dataModelRenderer = weeklyModelRenderer.getColorizedModel();
        model.addAttribute("scheduleWeeklyRenderer", dataModelRenderer);
        
        model.addAttribute("value", "Weekly view by " + currentDay);
   
        return "schedule";
    }
    
        
    @RequestMapping(value="/byMonth", method=RequestMethod.GET)
    public String printMonthlySchedule(Model model) throws ParseException {
        String[] headerScheduleTable = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};
        model.addAttribute("headerScheduleTable", headerScheduleTable);

        currentDay = (Date)new DateLib().changeDateFormat(new Date());
        startPeriod = DateLib.getFirstDayOfMounth(currentDay);
        endPeriod = DateLib.getLastDayOfMonth(currentDay);
        dataModel = monthlyModel.getModel(startPeriod, endPeriod);
        model.addAttribute("scheduleMonthly", dataModel);
        
        monthlyModelRenderer.setDate(startPeriod);
        dataModelRenderer = monthlyModelRenderer.getColorizedModel();
        model.addAttribute("scheduleMonthlyRenderer", dataModelRenderer);
        
        String[][] numbers = monthlyModelRenderer.getNumberDaysOfMonthModel();
        model.addAttribute("numberDaysOfMonthModel", numbers);
        
        model.addAttribute("value", "Monthly view by "+ currentDay);

        return "schedule";
    }
}
