package com.ghub.psi.controller;

import com.ghub.psi.domain.User;
import com.ghub.psi.service.IUserService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@RequestMapping("/registration")
@Controller
public class UserController {
    @Autowired
    private IUserService userService;
    
    @Autowired
    private Validator userValidator;
    
    @InitBinder
    private void initBinder(WebDataBinder binder) {
        binder.setValidator(userValidator);
    }
        
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(Model model) {
        model.addAttribute(new User());
        return "registration";
    }
    
    @RequestMapping(method=RequestMethod.POST)
    public String submitForm(@ModelAttribute("user") @Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("message", "Registration was not successful. Check your input data.");
	} else {
            user.setRoles("ROLE_USER");
            user.setActive("Y");
            userService.addUser(user);
            model.addAttribute("message", "Registration successfully completed. You can log on now. Good luck!");
	}		
        return "registration";
    }
}