package com.ghub.psi.controller;

import com.ghub.psi.domain.Task;
import com.ghub.psi.domain.User;
import com.ghub.psi.schedule.models.DateLib;
import com.ghub.psi.service.ITaskService;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/chat")
@Controller
public class ChatController {
    private User user; 
    @Autowired
    private ITaskService taskService;
     
    @RequestMapping("/create")
    public String create(Model model) throws ParseException {
        user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = user.getFirstname().length() > 0 ? user.getFirstname() : user.getLogin();
        if (!user.getRoles().equals("ROLE_ADMIN")) {
            Date consultationDate = checkUserSchedule(user.getUserId());  
            if (consultationDate == null) {
                String message = "Sorry " + userName + ", but we havn\'t records about you at now";
                model.addAttribute("message", message);
                return "info";
            } else {
                model.addAttribute("consultationDate", consultationDate);
            }
        }
        return "chat";
    }
    
    private Date checkUserSchedule(int userId) throws ParseException{
        Date recordDate = null; 
        Date currentDate = (Date)new DateLib().changeDateFormat(new Date());
        Date currentDay =  currentDate; 
        currentDay.setHours(0);
        currentDay.setMinutes(0);
        currentDay.setSeconds(0);
        Date startPeriod = currentDate; 
        Date endPeriod = DateLib.addDays(currentDate, 1);
        List<Task> list = taskService.listTaskByPeriod(startPeriod, endPeriod); 
        currentDate = new Date();
        boolean flag = false;
        for (Task task  : list) {
            if(userId == task.getUser().getUserId()) { 
                if (currentDate.getHours() == task.getDateRemind().getHours()  
                        && Math.abs(task.getDateRemind().getMinutes() - currentDate.getMinutes()) < 15
                        || currentDate.getHours() == task.getDateRemind().getHours() - 1  
                        && Math.abs(task.getDateRemind().getMinutes() - currentDate.getMinutes()) > 45) {
                
                    flag = true; 
                    recordDate = task.getDateRemind();
                    break;
                } else {
                    flag = false; 
                }  
            }  
        } 
        return flag == true ? recordDate : null;
    }
}
