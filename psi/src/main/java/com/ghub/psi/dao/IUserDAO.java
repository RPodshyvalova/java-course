package com.ghub.psi.dao;

import java.util.List;
import com.ghub.psi.domain.User;

public interface IUserDAO {
    
    public void addUser(User user);
    
    public List<User> listUser();
    
    public void removeUser(Integer id);
}
