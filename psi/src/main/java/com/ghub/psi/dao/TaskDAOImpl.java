package com.ghub.psi.dao;

import com.ghub.psi.domain.Task;
import com.ghub.psi.schedule.models.DateLib;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("iTaskDAO")
public class TaskDAOImpl implements ITaskDAO {
    @Autowired
    private SessionFactory sessionFactory;
 
    @Override
    public void addTask(Task task) {
        sessionFactory.getCurrentSession().save(task);
    }

    @Override
    public List<Task> listTask() {
        return sessionFactory.getCurrentSession().createQuery("from Task").list();    
    }
    
    @Override
    public void removeTask(Integer id) {
        Task  task = (Task) sessionFactory.getCurrentSession().load(Task.class, id);
	if (null != task) {
            sessionFactory.getCurrentSession().delete(task);
	}    
    }

    @Override
    public List<Task> listTaskByPeriod(Date startPeriod, Date endPeriod) {
        List<Task> list = null;
        try {
            list = sessionFactory.getCurrentSession().createQuery("from Task where "
                    + "(dateremind >= STR_TO_DATE(:startPeriod, '" + DateLib.mySqlFormatDate + "') "
                    + "AND dateremind <= STR_TO_DATE(:endPeriod, '" + DateLib.mySqlFormatDate + "')) "
                    + "ORDER BY dateremind")
                    .setParameter("startPeriod", String.valueOf(new DateLib().valueToString(startPeriod)))
                    .setParameter("endPeriod",  String.valueOf(new DateLib().valueToString(endPeriod)))
                    .list();    
        } catch (ParseException ex) {
            Logger.getLogger(TaskDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }
}
