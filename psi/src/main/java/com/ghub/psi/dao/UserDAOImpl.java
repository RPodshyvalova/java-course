package com.ghub.psi.dao;

import com.ghub.psi.domain.User;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("iUserDAO")
public class UserDAOImpl implements IUserDAO {
    @Autowired
    private SessionFactory sessionFactory;
    
    @Override
    public void addUser(User user) {
        sessionFactory.getCurrentSession().save(user);
    }

    @Override
    public List<User> listUser() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    @Override
    public void removeUser(Integer id) {
        User  user = (User) sessionFactory.getCurrentSession().load(User.class, id);
	if (null != user) {
            sessionFactory.getCurrentSession().delete(user);
	}
    }
}
