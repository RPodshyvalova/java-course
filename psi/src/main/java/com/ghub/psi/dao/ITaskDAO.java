package com.ghub.psi.dao;

import com.ghub.psi.domain.Task;
import java.util.Date;
import java.util.List;

public interface ITaskDAO {
    
    public void addTask(Task task);
    
    public List<Task> listTask();
        
    public List<Task> listTaskByPeriod(Date startPeriod, Date endPeriod);
    
    public void removeTask(Integer id);    
}
