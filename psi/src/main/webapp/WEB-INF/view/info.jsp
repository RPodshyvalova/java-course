<%@page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<c:url value="/resources/style.css" />" type="text/css" rel="stylesheet" />
    </head>
    <body>
        <h1>Information page:</h1>
        <c:if test="${! empty message}">
            <p class="error">
                <c:out value="${message}" />
            </p>
        </c:if>
    </body>
</html>
