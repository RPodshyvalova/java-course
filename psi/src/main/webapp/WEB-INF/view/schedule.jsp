<%@page language="java" contentType="text/html; charset=utf8" pageEncoding="utf8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page session="true"%>
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <link href="<c:url value="/resources/style.css" />" type="text/css" rel="stylesheet" />
    </head>
<!--    body   -->
    <body>
        <header>
            <security:authorize access="isAnonymous()">
                <div id="login">
                    <form method="POST" action="/psi/j_spring_security_check">
                        <input type="submit" value="" id="buttonLogin" />
                        <input type="text" name="j_password" placeholder="password" class="loginForm" />
                        <input type="text" name="j_username"  placeholder="login" class="loginForm" />
                    </form>
                </div>
            </security:authorize>
            <div id="logo">
                <h1 class="title">Dr. Nobodys<br>psychological<br>practice</h1>
            </div>
            <div id="menu">
                <ul>
                    <li><a href="<c:url value="/" />">Home</a></li>
                    <li><a href="<c:url value="/registration/create" />">Registration</a></li>
                    <li><a href="#" style="border: solid 1px #ffffff; padding:2px;">Schedule</a></li>
                    <security:authorize access="isAuthenticated()">
                        <li><a href="<c:url value="/chat/create" />">Chart</a></li>
                    </security:authorize>
                    <security:authorize access="isAnonymous()">
                        <li><a href="<c:url value="/signin-failure" />">Chart</a></li>
                    </security:authorize>
                    <li><a href="#">Contacts</a></li>
                </ul>
            </div>
        </header>
        
        <div id="main"> 
            <div style="width: 98%; height:50px;  background-color: #deeefd;">
                <div style="position:relative; float:left; width:50%; padding-left: 2%; margin-top: -12px;">
                    <security:authorize access="isAuthenticated()">
                        <form action="<c:url value="/schedule/create" />" method="POST">
                            <input type="text" name="date" id="date" placeholder="format 25.03.2015 18:17:00">
                            <input type="text" name="task" id="task" placeholder="comments">
                            <input type="submit"  value="" id="buttonOk">
                        </form>
                        <c:if test="${!empty createTask}">
                            <c:out value="${createTask}" />
                        </c:if>    
                    </security:authorize>
                    <security:authorize access="isAnonymous()">  
                        <h1 style="color: #df2936; font-size: 10pt; padding-top: 8px; padding-left: 2%; font-weight: normal;">Sign in for ability schedule an appointment</h1>
                    </security:authorize>
                </div>
                <div style="position:relative; float:right; width: 36%; padding-top: 4px;">
                    <p class="titleTxt">Schedule views:
                        <a href="<c:url value="/schedule/byDay" />">   <span class="scheduleViewLink">DAY</span></a>
                        <a href="<c:url value="/schedule/byWeek" />">  <span class="scheduleViewLink">WEEK</span></a>
                        <a href="<c:url value="/schedule/byMonth" />"> <span class="scheduleViewLink">MONTH</span></a>
                    </p>
                </div> 
 
            </div>
            <br/>
            <c:if test="${!empty value}">
                <c:out value="${value}" />
            </c:if>    
            <br/>
            <c:if test="${!empty scheduleDaily}">
                <c:set var="array" value="${scheduleDaily}"/>
                <c:set var="arrayDailyCellColor" value="${scheduleDailyRenderer}"/>
                <table class="scheduleTable"> 
                    <tr>
                        <c:if test="${!empty headerScheduleTable}">
                            <c:forEach items="${headerScheduleTable}" var="title">
                                <th><c:out value="${title}" /></th>
                            </c:forEach>
                       </c:if>
                    </tr>
                    <c:forEach var="i" begin="0" end="23">
                        <tr>
                            <c:forEach var="j" begin="0" end="1">
                                <td bgcolor="${arrayDailyCellColor[i][j]}"><p class="fraseTxt">${array[i][j]}</p></td>
                             </c:forEach> 
                        </tr>
                    </c:forEach>
                </table>
            </c:if> 

            <c:if test="${!empty scheduleWeekly}">
                <c:set var="array" value="${scheduleWeekly}"/>
                <c:set var="arrayWeeklyCellColor" value="${scheduleWeeklyRenderer}"/>
                <table class="scheduleTable"> 
                    <tr>
                        <c:if test="${!empty headerScheduleTable}">
                           <c:forEach items="${headerScheduleTable}" var="title">
                                <th><c:out value="${title}" /></th>
                           </c:forEach>
                       </c:if>
                    </tr>
                    <c:forEach var="i" begin="0" end="23">
                        <tr>
                            <c:forEach var="j" begin="0" end="7">
                                <td bgcolor="${arrayWeeklyCellColor[i][j]}"><p class="fraseTxt">${array[i][j]}</p></td>
                             </c:forEach> 
                        </tr>
                    </c:forEach>
                </table>
            </c:if> 

            <c:if test="${!empty scheduleMonthly}">
                <c:set var="array" value="${scheduleMonthly}"/>
                <c:set var="arrayMonthlyCellColor" value="${scheduleMonthlyRenderer}"/>
                <table class="scheduleTable" height="300px"> 
                    <tr>
                        <c:if test="${!empty headerScheduleTable}">
                           <c:forEach items="${headerScheduleTable}" var="title">
                                <th height="40px"><c:out value="${title}" /></th>
                           </c:forEach>
                       </c:if>
                    </tr>
                    <c:forEach var="i" begin="0" end="5">
                        <tr height="50px">
                            <c:forEach var="j" begin="0" end="6">
                                <td bgcolor="${arrayMonthlyCellColor[i][j]}">${numberDaysOfMonthModel[i][j]} <p class="fraseTxt">${array[i][j]}</p></td>
                             </c:forEach> 
                        </tr>
                    </c:forEach>
                </table>
            </c:if>     
        </div>
    </body>
</html>
