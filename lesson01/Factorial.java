package lesson01;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;

public class Factorial {
    public static void main(String[] args){
        try {
               for(;;){
                    BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));    
                    System.out.print("\nPlease, input number and press Enter key for counting \nor type Exit for exit: ");
                    String inputValue=reader.readLine();
                    if(!inputValue.equals("Exit")){
                       if(java.util.regex.Pattern.matches("\\d+", inputValue)){ 
                            int f=Integer.parseInt(inputValue);
                            BigInteger fact = BigInteger.ONE;
                            for(int j=f; j>1; j--){
                                   fact = fact.multiply(BigInteger.valueOf(j)); 
                                }
                                System.out.println("Factorial value for number " + f + " = " + fact.toString());
                                System.out.println();
                           
                        }else {System.out.println("Dont correct input value. Try one more time."); }
                    }else {System.out.println("\nBye."); System.exit(0);}
                } 
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
}