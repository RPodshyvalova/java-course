package lesson01;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NumWriteByString {
   public static void main(String[] args){
        try {
           for(;;){
                    String answer = null;
                    BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));    
                    System.out.print("\nPlease, input number and press Enter key for counting \nor type Exit for exit: ");
                    String inputValue=reader.readLine();
                    if(!inputValue.equals("Exit")){
                       if(java.util.regex.Pattern.matches("\\d+", inputValue)){ 
                            int i=Integer.parseInt(inputValue);
                            String A[]  = {"zero","one","two","three","four","five","six","seven","eight","nine"};
                            System.out.println("First variant is " + A[i]);
                          
                            switch(i){
                                default : answer="Sory, down\'t understand your input. It\'s must be 0..9";
                                case 0 : answer ="zero";   break;
                                case 1 : answer ="one";    break;
                                case 2 : answer ="two";    break;
                                case 3 : answer ="three";  break;
                                case 4 : answer ="four";   break; 
                                case 5 : answer ="five";   break;
                                case 6 : answer ="six";    break;
                                case 7 : answer ="seven";  break;
                                case 8 : answer ="eight";  break;
                                case 9 : answer ="nine";   break;
                            }
                            System.out.println("Second variant is " + answer);
                       
                            if (i ==0) { answer ="zero";} 
                            if (i ==1) { answer ="one";}    
                            if (i ==2) { answer ="two";}   
                            if (i ==3) { answer ="three";} 
                            if (i ==4) { answer ="four";}  
                            if (i ==5) { answer ="five";}  
                            if (i ==6) { answer ="six";}   
                            if (i ==7) { answer ="seven";} 
                            if (i ==8) { answer ="eight";} 
                            if (i ==9) { answer ="nine";}  
                            System.out.println("Third variant is " + answer);       
                           
                        }else {System.out.println("Dont correct input value. Try one more time.\n"); }
                    }else {System.out.println("\nBye."); System.exit(0);}
                } 
        } catch (IOException ex) {
            Logger.getLogger(NumWriteByString.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
}
