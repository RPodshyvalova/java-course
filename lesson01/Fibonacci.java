package lesson01;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Fibonacci {
    public static void main(String[] args){
        try {
               for(;;){
                    BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));    
                    System.out.print("\nPlease, input number and press Enter key for counting \nor type Exit for exit: ");
                    String inputValue=reader.readLine();
                    if(!inputValue.equals("Exit")){
                       if(java.util.regex.Pattern.matches("\\d+", inputValue)){ 
                            int fiboCount=Integer.parseInt(inputValue);
                            BigInteger x = BigInteger.ZERO;
                            BigInteger y = BigInteger.ONE;
                            BigInteger result = BigInteger.ZERO;
                            System.out.print("Number of Fibonacci is ");
                                for(int i=0; i<fiboCount; i++){
                                   System.out.print(result+" ");
                                   result = x.add(y);
                                   y=x;
                                   x=result;
                                }
                                System.out.println();
                           
                        }else {System.out.println("Dont correct input value. Try one more time.\n"); }
                    }else {System.out.println("\nBye."); System.exit(0);}
                } 
        }  catch (IOException ex) {
            Logger.getLogger(Fibonacci.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
