package lesson09;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "Tree", urlPatterns = {"/Tree"})
public class Tree extends HttpServlet {
//     
//    protected void init(HttpServletRequest request, HttpServletResponse response){
//        ServletContext context = getServletContext(); 
//        context.setAttribute( "storeDirectory", new String("D:/store/")); 
//    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet treeManager</title>");    
            out.println("<link href='style.css' type='text/css' rel='stylesheet' />");
            out.println("</head>");
            out.println("<body>");
   
            Path path = Paths.get(request.getParameter("path")); 
            PrintFileVisitor visitor = new PrintFileVisitor(request.getContextPath());
            Files.walkFileTree(path, (FileVisitor<? super Path>) visitor);

            out.println(visitor.printTree());
            out.println("</body>");
            out.println("</html>");
        }
    }
}
