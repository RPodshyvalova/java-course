package lesson09;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;

class PrintFileVisitor implements FileVisitor<Path> {
    private static final String DIST = "----";
    private static final int SIZE = 4;
    private static ArrayList<String> list;
    private StringBuffer indention;
    private String path;
 
    public PrintFileVisitor(String path) {
        indention = new StringBuffer();
        list = new ArrayList<>();
        this.path = path;
    }

    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        list.add(indention.toString() + "<img src='" + path + "/images/folder.png' width='30px' alt='image' />" + dir.getFileName() + "<br>");
        indention.append(DIST);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        indention.setLength(indention.length() - SIZE);
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        list.add(indention.toString() + file.getFileName() + "<br>");
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
        list.add(indention.toString() + file.getFileName() + "<br>");
        return FileVisitResult.CONTINUE;
    }

    public String printTree() {
        StringBuilder string = new StringBuilder();
        for (String element: list) {
            string.append(element);
        }
        return string.toString();
    }
}
