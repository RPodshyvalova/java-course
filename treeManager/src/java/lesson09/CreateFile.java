package lesson09;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "CreateFile", urlPatterns = {"/CreateFile"})
public class CreateFile extends HttpServlet {
   
   @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (FilesOperations.writeFile(request.getParameter("fileName"), request.getParameter("contents"))) {
                out.println("The file is successfully created.");
            } else {
                out.println("The file isn\'t created.");
            }    
        }
    }

}
