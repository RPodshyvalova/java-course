package lesson09;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class FilesOperations {

    public static StringBuilder readFile(String fileName) throws IOException {
        File file = new File(fileName);
        StringBuilder text = new StringBuilder();
        String loadString;
        try (BufferedReader reader = new BufferedReader(new FileReader(file.getAbsoluteFile()))){
            while ((loadString = reader.readLine()) != null){
                text.append(loadString).append('\n');
            }
        } 
        return text;
    }
    
    public static boolean writeFile(String fileName, String text) throws IOException{
        boolean flag = false;
        File file = new File(fileName);
        if (!file.exists()) {
            flag = file.createNewFile();
        } 
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file.getAbsoluteFile()))){
            writer.write(text); 
            flag = true;
        }
        return flag;
    }
    
    public static boolean deleteFile(String fileName) throws IOException{
        boolean flag = false;
        File file = new File(fileName);
        if (file.exists()) {
            flag = file.delete();
        }
        return flag;
    }
}
