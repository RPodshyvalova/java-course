package lesson09;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ContentsFile", urlPatterns = {"/ContentsFile"})
public class ContentsFile extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            StringBuilder string =  FilesOperations.readFile(request.getParameter("fileName"));
            if(string.length() > 0){
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet Create</title>");   
                out.println("<link href='style.css' type='text/css' rel='stylesheet' />");
                out.println("</head>");
                out.println("<body>");
//                out.println("<form action='/treeManager/ReWriteFile' method='POST'>");
                out.println("<form action='/treeManager/ContentsFile' method='POST'>");
                out.println("<h1>Rewrite file:</h1>");
                out.println("<input type='text' name='fileName' value='" + request.getParameter("fileName") + "'><br>");
                out.println("<textarea name='contents'>");     
                out.println(string); 
                out.println("</textarea><br>");
                out.println("<input type='submit' name='rewrite' value='rewrire'>");
                out.println("</form>");
                
                if (request.getParameter("contents").length() > 0){
                    if (FilesOperations.writeFile(request.getParameter("fileName"), request.getParameter("contents"))) {
                        out.println("The file is successfully rewritten.");
                    } else {
                        out.println("The file isn\'t rewritten.");
                    }    
                }
       
                out.println("</body>");
                out.println("</html>");
            } else {
                out.println("<p>Couldn\'t read files content.</p>"); 
            }
        }   
    }

}
