package lesson09;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DeleteFile", urlPatterns = {"/DeleteFile"})
public class DeleteFile extends HttpServlet {
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            if (FilesOperations.deleteFile(request.getParameter("fileName"))) {
                out.println("The file is successfully deleted.");
            } else {
                out.println("The file isn\'t deleted.");
            }
        }   
    }
}
