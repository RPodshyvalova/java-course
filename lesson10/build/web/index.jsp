<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/paginationTag" prefix="pag"%>
<%@ page session="false" pageEncoding="UTF-8"%>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <h1>Navigation Page</h1>
    <br />
 
    <c:choose> 
        <c:when test="${empty param.page}">
            <pag:pagination resultsPerPage = "10" totalResults = "82" currentPage = "1" pageURL = "index.jsp?page=1"/>
        </c:when>
        <c:otherwise>
            <pag:pagination resultsPerPage = "10" totalResults = "82" currentPage = "${param.page}" pageURL = "index.jsp?page=${param.page}"/>
        </c:otherwise>
    </c:choose>
</body>
</html>