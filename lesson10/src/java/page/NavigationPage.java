package page;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.SimpleTagSupport;

public class NavigationPage extends SimpleTagSupport{
    private String resultsPerPage;
    private String totalResults;
    private String currentPage;
    private String pageURL;
    private Map<Integer, String> map;
   
 
    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        PageContext pageContext = (PageContext)getJspContext();

        fillMap();
        int pagesCount = (int) Math.ceil(Integer.valueOf(totalResults) * 1.0 / Integer.valueOf(resultsPerPage));
        int begin, end;  
        if(Integer.valueOf(currentPage) == 1 || currentPage == null ) {
            begin = 1; 
        } else {
            begin = (Integer.valueOf(currentPage) - 1) * Integer.valueOf(resultsPerPage) + 1;
        }
        end = begin + Integer.valueOf(resultsPerPage);

        try {
            StringBuilder context = new StringBuilder();
            context.append(
                    "<table>"
                        + "<thead>"
                        + "<tr>"
                        + "<th>id</th>"
                        + "<th>name</th>"
                        + "</tr>"
                        + "</thead>");

            for (int i=begin; i<end; i++) {
                context.append("<tr>"
                            + "<td>" + i + "</td>"
                            + "<td>" + map.get(i).toString() + "</td>"
                            + "</tr>");
                            
            }
            String numbers = begin + " - " + (end - 1);
            context.append(
                    "</table>"
                    + "<div class='controlArea'>"   
                    + "<p class='shadow'>" + numbers + "</p>"
                    + "<br>"
                    + "<br>");        
            if (Integer.valueOf(currentPage) > 1) {
                context.append("<a href=\"index.jsp?page=" + (Integer.valueOf(currentPage) - 1) + "\"> < </a>&nbsp;&nbsp;&nbsp;"); 
            } else {
                context.append("<span class=\"dontShow\"><</span>&nbsp;&nbsp;&nbsp;"); 
            }  
       
            for (int i=1; i<=pagesCount; i++) {
                if( Integer.valueOf(currentPage) == i ) {
                    context.append("<a href=\"index.jsp?page=" + i+ "\" class =\"currPage\"> " + i + "</a>&nbsp;&nbsp;&nbsp;"); 
                } else {
                    context.append("<a href=\"index.jsp?page=" + i+ "\"> " + i + " </a>&nbsp;&nbsp;&nbsp;");
                }
            }
            if (Integer.valueOf(currentPage) < pagesCount) {
                context.append("<a href=\"index.jsp?page=" + (Integer.valueOf(currentPage) + 1) + "\"> > </a>"); 
            } else {
                context.append("<span class=\"dontShow\">></span>&nbsp;&nbsp;&nbsp;"); 
            }
            context.append("</div>");
            pageContext.getOut().print(context);
          
            
        } catch(IOException ioException) {
            throw new JspException("Error: " + ioException.getMessage());
        } 
    }   

    private void fillMap(){
        map = new TreeMap<Integer, String>();
        for (int i=0; i<100; i++) {
            map.put(i, "Element_" + i);
        }
    }
    
    public void setResultsPerPage(String resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public void setPageURL(String pageURL) {
        this.pageURL = pageURL;
    }

}
