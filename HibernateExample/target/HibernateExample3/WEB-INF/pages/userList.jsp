<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="list" type="java.util.List<com.geekhub.model.User>"--%>

<html>
<body>
    <table>
        <tr>
            <th>Id</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Admin</th>
        </tr>
    <c:forEach var="user" items="${list}">
        <tr>
            <td>${user.id}</td>
            <td>${user.firstName}</td>
            <td>${user.lastName}</td>
            <td>${user.admin}</td>
            <td> <a href="http://localhost:8080/HibernateExample/user/delete/${user.id}">delete</td>
            <td> <a href="http://localhost:8080/HibernateExample/user/update/${user.id}">update</td>
        </tr>
    </c:forEach>
    </table>
</body>
</html>