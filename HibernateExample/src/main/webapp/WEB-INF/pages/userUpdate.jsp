<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<body>
    <form action="http://localhost:8080/HibernateExample/user/refresh" method="POST">
        <label>Id: <input type="text" name="id" value='<c:out value="${user.id}"/>' READONLY/></label><br/><br/>
        <label>First Name: <input type="text" name="firstName" value='<c:out value="${user.firstName}"/>'/></label><br/><br/>
        <label>Last Name: <input type="text" name="lastName" value='<c:out value="${user.lastName}"/>'/></label><br/><br/>
        <label>Admin: <input type="checkbox" name="admin" value='<c:out value="${user.admin}"/>' /></label><br/><br/>
        <input type="submit" value="Update">
    </form>
</body>
</html>