package com.geekhub.controller;

import com.geekhub.model.Groups;
import com.geekhub.model.GroupDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.List;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequestMapping("/group")
public class GroupController {

    @Autowired private GroupDao groupDao;

    @RequestMapping("/list")
    public String list(ModelMap model) {
        List<Groups> groups= groupDao.getGroups();
        model.addAttribute("list", groups);
	return "groupList";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String showCreate() {
        return "createGroup";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(String name) {
        Groups group = new Groups();
        group.setName(name);
        groupDao.saveGroup(group);
        return "redirect:/group/list";
    }
    
    @RequestMapping("/delete/{id}")
    public String deleteContact(@PathVariable("id") Integer groupId) {
        Groups group = groupDao.getGroup(groupId);
	groupDao.deleteGroup(group);
        return "redirect:/group/list";
    }
}