package com.geekhub.controller;

import com.geekhub.model.GroupDao;
import com.geekhub.model.Groups;
import com.geekhub.model.User;
import com.geekhub.model.UserDao;
import java.security.acl.Group;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired private UserDao userDao;
    @Autowired private SessionFactory sessionFactory;

    
    @RequestMapping("/list")
    public String list(ModelMap model) {
        List<User> users = userDao.getUsers();
        model.addAttribute("list", users);
	return "userList";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String showCreate() {
        return "createUser";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String create(String firstName, String lastName, boolean admin, String nameGroup) throws Exception {

        Groups group = new Groups();
        group.setName(nameGroup);

        User user = new User();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAdmin(admin);
        user.setGroups(group);

        group.setUsers(new ArrayList<User>());
        group.getUsers().add(user);
        if (userDao.checkUniqueUser(user) == null) {
            Session session = sessionFactory.openSession();
            try {
                session.beginTransaction();
                session.saveOrUpdate(group);
                session.saveOrUpdate(user);
                session.getTransaction().commit();
            } catch (HibernateException e) { 
                session.getTransaction().rollback();
                throw new Exception("Couldn\'t save user " + user.getFirstName()+ user.getLastName(), e);
            } finally {
                session.close();
            } 
        }
        return "redirect:/user/list";
    }
    
    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer userId) throws Exception {
        User user = userDao.getUser(userId);
	userDao.deleteUser(user);
        return "redirect:/user/list";
    }
    
    @RequestMapping("/update/{id}")
    public String update(ModelMap model, @PathVariable("id") Integer userId) throws Exception {
        User user = userDao.getUser(userId);
        model.addAttribute("user", user);
        return "userUpdate";
    }
     
    
   @RequestMapping(value = "/refresh", method = RequestMethod.POST)
    public String refresh(int id, String firstName, String lastName, boolean admin) throws Exception {
        User user = userDao.getUser(id);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setAdmin(admin);
        if (userDao.checkUniqueUser(user) == null) {
            userDao.refreshUser(user);
        }
        return "redirect:/user/list";
    }

}