package com.geekhub.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;

@Component
@SuppressWarnings("unchecked")
public class UserDao {

    @Autowired private SessionFactory sessionFactory;

    public List<User> getUsers() {
        Session session = sessionFactory.openSession();
        try {
            return (List<User>) session.createCriteria(User.class)
                    .addOrder(Order.asc("firstName"))
                    .list();
        } finally {
            session.close();
        }
    }

    public User getUser(int id) throws Exception {
        Session session = sessionFactory.openSession();
        User user;
        try {
            user  = (User)session.get(User.class, id);
        } catch (HibernateException e) { 
            session.getTransaction().rollback();
            throw new Exception("Couldn\'t get info about user " + id, e);
        } finally {
            session.close();
        }
        return user;
    }

    public void saveUser(User user) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(user);
            session.getTransaction().commit();
        } catch (HibernateException e) { 
            session.getTransaction().rollback();
            throw new Exception("Couldn\'t save user " + user.getFirstName()+ user.getLastName(), e);
        } finally {
            session.close();
        }
    }

    public void deleteUser(User user) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        } catch (HibernateException e) { 
            session.getTransaction().rollback();
            throw new Exception("Couldn\'t delete user " + user.getFirstName()+ user.getLastName(), e);
        } finally {
            session.close();
        }
    }
    
    public void refreshUser(User user) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.update(user); 
            session.getTransaction().commit();
        } catch (HibernateException e) { 
            session.getTransaction().rollback();
            throw new Exception("Couldn\'t update info about user " + user.getFirstName()+ user.getLastName(), e);
        } finally {
            session.close();
        }
    }
    
    public User checkUniqueUser(User user) throws Exception {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            Query query = session.createQuery("from User where firstName = :name and lastName = :surname");
            query.setString("name", user.getFirstName());
            query.setString("surname", user.getLastName());
            User uniqueUser = (User)query.uniqueResult();
            session.getTransaction().commit();
            return uniqueUser;
        } catch (HibernateException e) { 
            session.getTransaction().rollback();
            throw new Exception("Couldn\'t check user " + user.getFirstName()+ user.getLastName(), e);
        } finally {
            session.close();
        }
    }
}
