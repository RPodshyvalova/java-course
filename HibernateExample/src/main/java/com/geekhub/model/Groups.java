package com.geekhub.model;

import java.util.List;
import javax.persistence.*;
import org.hibernate.annotations.IndexColumn;

@Entity
@Table
public class Groups {
    @Id
    @GeneratedValue
    @Column(name="groups_id")
    private Integer id;
    @Column
    private String name;
    
    @OneToMany(mappedBy="groups")
    private List<User> users;
    
    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
