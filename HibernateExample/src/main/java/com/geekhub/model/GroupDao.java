package com.geekhub.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
@SuppressWarnings("unchecked")
public class GroupDao {
    @Autowired private SessionFactory sessionFactory;

    public List<Groups> getGroups() {
        Session session = sessionFactory.openSession();
        try {
            return (List<Groups>) session.createCriteria(Groups.class)
                    .addOrder(Order.asc("name"))
                    .list();
        } finally {
            session.close();
        }
    }

    public Groups getGroup(int id) {
        Session session = sessionFactory.openSession();
        Groups group;
        try {
            group  = (Groups)session.get(Groups.class, id);
        } finally {
            session.close();
        }
        return group;
    }

    public void saveGroup(Groups group) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.saveOrUpdate(group);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }

    public void deleteGroup(Groups group) {
        Session session = sessionFactory.openSession();
        try {
            session.beginTransaction();
            session.delete(group);
            session.getTransaction().commit();
        } finally {
            session.close();
        }
    }
}
