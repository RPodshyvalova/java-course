package org.geekhub.storage;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import org.geekhub.objects.Entity;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementation of {@link org.geekhub.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 It uses reflection to access objects fields and retrieve data to data to database tables.
 As an identifier it uses field id of {@link org.geekhub.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try(Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        int countRowsDeleted = 0;
        String params = keyValueFormatQueryParameter(entity);
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE " + params;
        try(Statement statement = connection. createStatement()) {
            System.out.println(sql);
            countRowsDeleted = statement.executeUpdate(sql);
        }
        return countRowsDeleted > 0;
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        String sql;
        int countRowsSave;
        Map<String, Object> data = prepareEntity(entity);
        
        if (entity.isNew()) {
            String nameCols = "";
            String paramsInsert = "";
            int count = 1;

            for(Map.Entry<String, Object> entry : data.entrySet()) {
                nameCols += entry.getKey();
                if(entry.getKey().equals("id")){
                    paramsInsert += "null";
                } else {
                    if (entry.getValue().getClass().toString().equals("class java.lang.String")){
                        paramsInsert +=  "'" + entry.getValue()+"'";    
                    } else {
                        paramsInsert += entry.getValue();
                    } 
                }
                if (count!=data.size()){
                    nameCols +=  ", ";
                    paramsInsert +=  ", ";
                }
                count++;
            }
         
            sql = "INSERT INTO "+ entity.getClass().getSimpleName() + " (" + nameCols + ") VALUES (" + paramsInsert + ")";
            try(Statement statement = connection. createStatement()) {
                System.out.println(sql);
                countRowsSave = statement.executeUpdate(sql);
            }
        } else {
            String paramsUpdate = keyValueFormatQueryParameter(entity).replace("AND", ",");
            sql = "UPDATE " + entity.getClass().getSimpleName() + " SET " + paramsUpdate + " WHERE id =" + entity.getId().toString();
            try(Statement statement = connection. createStatement()) {
                System.out.println(sql);
                countRowsSave = statement.executeUpdate(sql);
            }
        }

        String printAnswer = countRowsSave > 0 ? "The data is stored." :  "The data is not stored.";
        System.out.println(printAnswer);

    }

    //converts object to data, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        //implement me
        Map<String, Object> map = new HashMap<>();
        Class<T> clazz = (Class<T>) entity.getClass();
        List<Field> listFields = listClazzFields(clazz);

        listFields.stream().forEach((Field field) -> {
             Annotation annotation = field.getAnnotation(org.geekhub.objects.Ignore.class);
                if (annotation == null ) {
                    try {
                        field.setAccessible(true);
                        map.put(field.getName(), field.get(entity));
                    } catch (IllegalArgumentException | IllegalAccessException ex) {
                        Logger.getLogger(DatabaseStorage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
        });
        return map;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> list = new ArrayList<>();
        List<Field> listFields = listClazzFields(clazz);
      
        while(resultset.next()){
            T object = clazz.getConstructor().newInstance();
            listFields.stream().forEach((Field field) -> {
                Annotation annotation = field.getAnnotation(org.geekhub.objects.Ignore.class);
                if (annotation == null ) {
                    field.setAccessible(true);
                    try {
                        try {
                            field.set(object, resultset.getObject(field.getName()));
                        } catch (IllegalArgumentException | IllegalAccessException ex) {
                            Logger.getLogger(DatabaseStorage.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (SQLException ex) {
                        Logger.getLogger(DatabaseStorage.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            });
            list.add(object);
            
        }
        return list.size()> 0 ? list : null;
    }
    
    private<T extends Entity> List<Field> listClazzFields(Class<T> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        Field[] fieldsSuperclass = clazz.getSuperclass().getDeclaredFields();
        List<Field> listFields = new ArrayList<>(); 
        Collections.addAll(listFields, fieldsSuperclass); 
        Collections.addAll(listFields, fields); 
        return listFields;
    }
    
    private<T extends Entity> String keyValueFormatQueryParameter(T entity) throws Exception{
        String params = "";
        int count = 1;
        Map<String, Object> data = prepareEntity(entity);
        try {
            for(Map.Entry<String, Object> entry : data.entrySet()) {
                if (entry.getValue().getClass().toString().equals("class java.lang.String")){
                    params +=  " " + entry.getKey()  + "='" + entry.getValue()+"'";
                } else {
                    params +=  " " + entry.getKey()  + "=" + entry.getValue();
                }
                
                if (count != data.size()){
                    params +=  " AND ";
                }
                count++;
            }
        } catch (Exception ex) {
            Logger.getLogger(DatabaseStorage.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return params;
    }
}
