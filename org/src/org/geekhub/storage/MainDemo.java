package org.geekhub.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import org.geekhub.objects.User;

public class MainDemo {
    public static void main(String args[]) throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException, Exception{
        String url = "jdbc:mysql://localhost:3307/";
//        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "geekdb";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root"; 
        String password = "7777777";
        Class.forName(driver).newInstance();
        try (Connection connection = DriverManager.getConnection(url+dbName,userName,password)){
            viewTable(connection);
        }
    }

    public static void viewTable(Connection connection) throws SQLException, Exception {
        DatabaseStorage ds = new DatabaseStorage(connection);
//        System.out.println("Don\'t forget invite persons to a lunch.");
//        User userSave = new User();
//        userSave.setName("Olya Dergounova");
//        userSave.setAge(49);
//        userSave.setBalance(1000d);
//        userSave.setAdmin(Boolean.TRUE);
//        ds.save(userSave);
//
        User userSave2 = new User();
        userSave2.setName("Stevie Ballmer");
        userSave2.setAge(58);
        userSave2.setBalance(1000d);
        userSave2.setAdmin(Boolean.TRUE);
//        ds.save(userSave2);
        
        userSave2.setId(2);
        userSave2.setBalance(1001d);
        ds.save(userSave2);
        
        System.out.println("All users from database: ");
        List<User> list;
        if((list = ds.list(User.class)).size() > 0) {
            list.stream().forEach((allUser) -> {
                System.out.println("\t" + allUser.toString());
            });        
        }
    }
}
