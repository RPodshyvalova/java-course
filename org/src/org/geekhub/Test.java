package org.geekhub;

import org.geekhub.objects.Cat;
import org.geekhub.objects.User;
import org.geekhub.storage.DatabaseStorage;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

public class Test {
    public static void main(String[] args) throws Exception {
        Connection connection = createConnection();
        DatabaseStorage storage = new DatabaseStorage(connection);
//        Storage storage = new DatabaseStorage(connection);
        List<Cat> cats = storage.list(Cat.class);
        for (Cat cat : cats) {
            storage.delete(cat);
        }
        cats = storage.list(Cat.class);
        if (!cats.isEmpty()) throw new Exception("Cats should not be in database!");

        for(int i = 1; i <= 20; i++) {
            Cat cat = new Cat();
            cat.setName("cat" + i);
            cat.setAge(i);
            storage.save(cat);
        }
       
        cats = storage.list(Cat.class);
        if (cats.size() != 20) throw new Exception("Number of cats in storage should be 20!");

        User user = new User();
        user.setAdmin(true);
        user.setAge(23);
        user.setName("Victor");
        user.setBalance(22.23);
        storage.save(user);

        User user1 = storage.get(User.class, user.getId());
        if (!user1.getName().equals(user.getName())) throw new Exception("Users should be equals!");

        user.setAdmin(false);
        storage.save(user);

        User user2 = storage.get(User.class, user.getId());
        if (!user.getAdmin().equals(user2.getAdmin())) throw new Exception("Users should be updated!");

        storage.delete(user1);

        User user3 = storage.get(User.class, user.getId());

        if (user3 != null) throw new Exception("User should be deleted!");

        connection.close();
    }

    private static Connection createConnection() throws Exception {
        //implement me: initiate connection
                String url = "jdbc:mysql://localhost:3307/";
//        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "geekdb";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root"; 
        String password = "7777777";
        Class.forName(driver).newInstance();

        Connection connection = DriverManager.getConnection(url+dbName,userName,password);  
        return connection; 
    }
}
