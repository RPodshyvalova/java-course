package com.logic;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@WebServlet("/Translator")
public class Translator extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      
        ServletContext servletContext = request.getServletContext();
//        WebApplicationContext context = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
        ApplicationContext context = (ApplicationContext) servletContext.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
 
        
        ResourceLoader loader = (ResourceLoader) context.getBean("resourceLoader");
        StringBuilder filesContent =  loader.readFile(request.getParameter("fileName"));
          
        Dictionary dictionary  = (Dictionary) context.getBean("dictionary");
        StringBuilder  translateContent = dictionary.translateWord(filesContent);
        
        request.setAttribute("sourceContent", "Source text:    " + filesContent);    
        request.setAttribute("translatedContent", "Translated text:    " + translateContent);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("index.jsp");    
        requestDispatcher.forward(request, response);
    }   
}
