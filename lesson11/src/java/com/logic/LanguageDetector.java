package com.logic;

import org.springframework.stereotype.Component;

@Component
public class LanguageDetector {
    private static final String RU = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    private static final String ENG = "abcdefghijklmnopqrstuvwxyz";
    
    public boolean detectLang(String text, String lang){
        boolean flag = false;
        String alphabet = null;
        
        switch (lang) {
            case "RU" : 
                alphabet = RU;
                break;
            case "ENG" :  
                alphabet = ENG;
                break;
        }
        
        text = text.toLowerCase();
        for (int i=0; i<text.length(); i++){
            if (alphabet.indexOf(text.charAt(i)) < 0) {
                flag = false; 
                break;
            } else {
                flag = true;  
            }
        }    
        return flag;
    }
}
