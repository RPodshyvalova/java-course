package com.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import org.springframework.stereotype.Component;

@Component
public class ResourceLoader {
    public StringBuilder readFile(String fileName) throws IOException {
        File file = new File(fileName);
        StringBuilder text = new StringBuilder();
        String loadString;
        try (BufferedReader reader = new BufferedReader(new FileReader(file.getAbsoluteFile()))){
            while ((loadString = reader.readLine()) != null){
                text.append(loadString).append('\n');
            }
        } 
        return text;
    }
}
