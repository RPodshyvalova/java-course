package com.logic;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Dictionary {
    
    @Autowired
    private LanguageDetector detector; 
    
    public StringBuilder translateWord(StringBuilder text){
        Map<String, String> vocabularySource = null;
      
        Map<String, String> vocabularyEng = new HashMap<>();  
        vocabularyEng.put("hello","привіт"); 
        vocabularyEng.put("friends","друзі"); 
        
        Map<String, String> vocabularyRu = new HashMap<>();  
        vocabularyRu.put("привет","привіт"); 
        vocabularyRu.put("друзья","друзі"); 
        
        String tmp = text.toString().trim();

        boolean isEng = detector.detectLang(tmp.substring(0,2), "ENG");
        boolean isRu = detector.detectLang(tmp.substring(0,2), "RU");
     
        StringBuilder result = new StringBuilder();
        if (isEng == true) {
            vocabularySource = vocabularyEng;
        }
        if (isRu == true) {
            vocabularySource = vocabularyRu;
        }
        
        boolean flag;
        String words[] =  text.toString().split(" ");
        for (String word : words) {
            flag = false;
            for (Map.Entry entry : vocabularySource.entrySet()) {
                if (entry.getKey().equals(word.trim())) {
                    result.append(entry.getValue()).append(" "); 
                    flag = true;
                    break;
                } 
            }
            if (!flag) {
                result.append(word).append(" "); 
            }
        }
        return result;
    }
}
