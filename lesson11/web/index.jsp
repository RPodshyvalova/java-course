<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <link rel="stylesheet" type="text/css" href="style.css">
    </head>
    <body>
        <form action="http://localhost:8080/lesson11/Translator" method="GET">
            <p>File:&nbsp;&nbsp;
                <input type="text" name="fileName">
                <input type="submit" class="customButton" value="">
               
            </p>  
        </form>
    
        <div class="sourceTxt"> 
            <c:out value="${requestScope.sourceContent}" />
        </div>
        <div class="translatedTxt">
            <c:set var="content" value="${requestScope.translatedContent}" />
            <c:if test="${content != null}">
                <c:out value="${content}" />
            </c:if>
        </div>      
    </body>
</html>
