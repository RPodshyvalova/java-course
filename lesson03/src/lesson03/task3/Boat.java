package lesson03.task3;

import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Boat extends Vehicle {
    private DieselEngine dieselEngine;
    private GasTank gasTank;
    private Propeller propeller; 
  
    Boat(){
        super();
        dieselEngine = null;
        gasTank = null;
        propeller = null;
    }
 
    Boat(String model, int currentSpeed, int serialNumber, String manufacturer, int yearOfManufacture, 
         DieselEngine dieselEngine, GasTank gasTank, Propeller propeller){
       super(model, currentSpeed, serialNumber, manufacturer, yearOfManufacture); 
       this.dieselEngine = dieselEngine;
       this.gasTank = gasTank;
       this.propeller = propeller;
    }
    
    @Override
    void driveSomeKm(int valueKm, int speed) {
       System.out.println("Boat drive some km");
    }

    @Override
    void increaseEnergy(Float value) {
        try {
            this.gasTank.fillEnergySource(value);
        } catch (FillEnergySourceMethodException ex) {
            System.out.println(ex.toString());
        }
        
    }

    @Override
    String showBalanceEnergy() {
       return String.valueOf(this.gasTank.getCurrentCapacity());
    }

    
    @Override
    String showCurrentSpeed() {
        return String.valueOf(this.getCurrentSpeed());
    }

    @Override
    public void accelerate(int value) throws AccelerateMethodException{
        if( this.getCurrentSpeed() + value < this.dieselEngine.getMaxSpeed()){
            this.setCurrentSpeed(this.getCurrentSpeed() + value);
        }else {
            this.setCurrentSpeed(this.dieselEngine.getMaxSpeed());
            throw new AccelerateMethodException("Reached the limit speed " + this.dieselEngine.getMaxSpeed());
        }
    }

    @Override
    public void brake() {
        System.out.println("Car brake");
    }

    @Override
    public void turn() {
        System.out.println("Car turn");
    }

    @Override
    public String toString() {
        return super.toString() + "Boat{" + "dieselEngine=" + dieselEngine + ", "
               + "gasTank=" + gasTank + ", propeller=" + propeller + '}';
    }
}
