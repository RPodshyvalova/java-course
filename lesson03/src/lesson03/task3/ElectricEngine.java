package lesson03.task3;

import java.util.Objects;

public class ElectricEngine implements ForceProvider {
    private String model;
    private int power;
    private int maxSpeed;  
    private float energyConsumption;
    private float accelerationTo100;
    private int serialNumber;
    private String manufacturer;
    private static int number = 0;

    ElectricEngine(){
        model = "";
        power = 0;
        maxSpeed = 0;  
        accelerationTo100 = 0.0F;
        number++;
        serialNumber = number;
        manufacturer = "";
    }
    
    ElectricEngine(String model, int power, int maxSpeed, float accelerationTo100, String manufacturer){
        this.model = model;
        this.power = power;
        this.maxSpeed = maxSpeed;  
        this.accelerationTo100 = accelerationTo100;
        number++;
        this.serialNumber = number;
        this.serialNumber = serialNumber;
    }

    @Override
    public void run() {
        System.out.println("Electric engine is runing");
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public float getEnergyConsumption() {
        return energyConsumption;
    }

    public void setEnergyConsumption(float energyConsumption) {
        this.energyConsumption = energyConsumption;
    }

    public float getAccelerationTo100() {
        return accelerationTo100;
    }

    public void setAccelerationTo100(float accelerationTo100) {
        this.accelerationTo100 = accelerationTo100;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "ElectricEngine{" + "model=" + model + ", power=" + power + ", maxSpeed=" + maxSpeed 
                + ", energyConsumption=" + energyConsumption + ", accelerationTo100=" + accelerationTo100 
                + ", serialNumber=" + serialNumber + ", manufacturer=" + manufacturer + '}';
    }
   
}
