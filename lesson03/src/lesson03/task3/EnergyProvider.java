package lesson03.task3;

class FillEnergySourceMethodException extends Exception{
    private String msg;
    
    FillEnergySourceMethodException(){
        msg = null;
    }
    
    FillEnergySourceMethodException(String msg){
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "**************** FillEnergySourceMethodException - " + "msg = " + msg + " ***************";
    }
}
 
public interface EnergyProvider {
    void fillEnergySource(Float value) throws FillEnergySourceMethodException;
    void expendEnergySource();
    
}

