package lesson03.task3;

import java.util.Objects;

public class Propeller implements ForceAcceptor {
    private String model;
    private String propellersStep;
    private int size;
    private String materialMadeOf;
    private int serialNumber;
    private String manufacturer;
    
    {   model = "";
        propellersStep = "";
        serialNumber = 0;
        size = 0;
        materialMadeOf = "";
        manufacturer = "";
    }
    
    Propeller (){
    }
   
    Propeller(String model, String propellersStep, int size, String materialMadeOf, 
              int serialNumber, String manufacturer){
        this.model = model;
        this.propellersStep = propellersStep;
        this.serialNumber = serialNumber;
        this.size = size;
        this.materialMadeOf = materialMadeOf;
        this.manufacturer = manufacturer;
    }

    @Override
    public void rotate() {
        System.out.println("The propeller is rotating.");
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getPropellersStep() {
        return propellersStep;
    }

    public void setPropellersStep(String propellersStep) {
        this.propellersStep = propellersStep;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getMaterialMadeOf() {
        return materialMadeOf;
    }

    public void setMaterialMadeOf(String materialMadeOf) {
        this.materialMadeOf = materialMadeOf;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "Propeller{" + "model=" + model + ", propellersStep=" + propellersStep + ", size=" + size 
               + ", materialMadeOf=" + materialMadeOf + ", serialNumber=" + serialNumber 
               + ", manufacturer=" + manufacturer + '}';
    }
 }
