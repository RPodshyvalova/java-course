package lesson03.task3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class MainDemo {
    private static BufferedReader reader;
    private static String inputValue;
    
    private static void chooseActionForVechicle(Vehicle vehicle) throws IOException, AccelerateMethodException {
        reader = new BufferedReader(new InputStreamReader(System.in));  
        System.out.print("Choose action: "
                            + "\n1 - drive some km "
                            + "\n2 - increase energy for some units"      
                            + "\n3 - view residue energy"     
                            + "\n4 - increase speed in  some km/h"   
                            + "\n5 - for exit\n");
        inputValue = reader.readLine();
        switch(inputValue) {
            case "1": 
                System.out.println("Current speed is " + vehicle.getCurrentSpeed());
                System.out.println("Now balance energy is = " + vehicle.showBalanceEnergy());
                 try{
                    System.out.print("How many miles you want to drive: ");
                    reader = new BufferedReader(new InputStreamReader(System.in));   
                    inputValue = reader.readLine();
                    vehicle.driveSomeKm(Integer.valueOf(inputValue), 140);
                 }catch(DriveSomeKmMethodException e){
                    System.out.println(e.toString());
                } finally {
                    break;     
                }
                
            case "2": 
                System.out.print("How much energy to fill: ");
                reader = new BufferedReader(new InputStreamReader(System.in));   
                inputValue = reader.readLine();
                vehicle.increaseEnergy(Float.valueOf(inputValue));
                System.out.println("Now balance energy is = " + vehicle.showBalanceEnergy());
                break;
                
            case "3": 
                vehicle.showBalanceEnergy();
                System.out.println("Now balance energy is = " + vehicle.showBalanceEnergy());
                break;   
                
            case "4":
                System.out.print("How much you want to increase speed: ");
                reader = new BufferedReader(new InputStreamReader(System.in));   
                inputValue = reader.readLine();
                try{
                    vehicle.accelerate(Integer.valueOf(inputValue)); 
                    System.out.println("Now current speed is = " + vehicle.showCurrentSpeed());
                }catch(AccelerateMethodException e){
                    System.out.println(e.toString());
                } finally {
                    break;     
                }
            
            case "5": 
                System.out.println("\nGood luck!"); 
                System.exit(0);
        }
    }
    
    private static void showInfoByVechicle(Vehicle vehicle){
        System.out.println(vehicle.toString() + "\n");
    }
    
    public static void main(String[] args) throws CloneNotSupportedException, IOException, AccelerateMethodException {
        Vehicle[] vehicle = new Vehicle[3];
        System.out.println("\nHi. Let\'s do a test drive of this baby?");
        vehicle[0] = new Car("Bentley Continental Supersports", 140, 1, "Bentley Motors Ltd.", 2010, 
                              new DieselEngine(621, 329, 16.3F, 3.9F), 
                              new Chassis(4, new Wheel("wheel", 18, "Carbon steel", 1, " BBS")), 
                              new GasTank(90F, 60F));
        vehicle[1] = new SolarPoweredCar();
        vehicle[2] = new Boat();
        for(;;){
            System.out.println();
            showInfoByVechicle(vehicle[0]);
            chooseActionForVechicle(vehicle[0]);
        }
/*            
            reader = new BufferedReader(new InputStreamReader(System.in));  
            System.out.println("\nHi. Want to test drive?");
            System.out.print("\nDecide what you want to test and make your choice: "
                            + "\n1 - Bentley Continental Supersports"
                        //    + "\n2 - "      // for solar car
                        //    + "\n3 - "      // for boat
                            + "\n2 - for exit\n");
            inputValue = reader.readLine();
            System.out.println();
            switch(inputValue) {
            case "1":  showInfoByVechicle(vehicle[0]);
                       chooseActionForVechicle(vehicle[0]);
                       break;
            case "2": System.out.println("\nGood luck!"); 
                       System.exit(0);
            
            }    
*/        
        
                
    }    
}
