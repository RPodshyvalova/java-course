package lesson03.task3;

import java.util.Objects;

public class GasTank implements EnergyProvider{
    private String model;
    private float maxCapacity;
    private float currentCapacity;
    private float weight;
    private int serialNumber;
    private String manufacturer;
  
    GasTank(){
        model = "";
        maxCapacity = 0.0F;
        currentCapacity = 0.0F;
        weight = 0.0F;
        serialNumber = 0;
        manufacturer = "";
    }
    
    GasTank(float maxCapacity, float currentCapacity){
        this();
        this.maxCapacity = maxCapacity;
        this.currentCapacity = currentCapacity;
    }
    
    GasTank(String model, float maxCapacity, float currentCapacity, float weight, int serialNumber, String manufacturer){
        this.model = model;
        this.maxCapacity = maxCapacity;
        this.currentCapacity = currentCapacity;
        this.weight =  weight;
        this.serialNumber = serialNumber;
        this.manufacturer =  manufacturer;
    }

    @Override
    public void fillEnergySource(Float value) throws FillEnergySourceMethodException {
        float difference = 0.0F;
        if(currentCapacity + value >= maxCapacity){
            difference = maxCapacity - currentCapacity;
            currentCapacity += difference; 
            throw new FillEnergySourceMethodException("The tank is full");
        }else{ 
            currentCapacity += value; 
            System.out.println("The tank is filled with fuel on " + String.valueOf(value) + " l");
        }
    }

    @Override
    public void expendEnergySource() {
        System.out.println("The fuel level in the tank decreases.");
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(float maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public float getCurrentCapacity() {
        return Math.round(currentCapacity);
    }

    public void setCurrentCapacity(float currentCapacity) {
        this.currentCapacity = currentCapacity;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "GasTank{" + "model=" + model + ", maxCapacity=" + maxCapacity 
                + "l, currentCapacity=" + Math.round(currentCapacity) + "l, weight=" + weight 
                + ", serialNumber=" + serialNumber + ", manufacturer=" + manufacturer + '}';
    }
   
}
