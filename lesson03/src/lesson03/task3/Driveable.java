package lesson03.task3;

class  AccelerateMethodException extends Exception{
    private String msg;
  
    AccelerateMethodException(){
        msg = null;
    }
    
    AccelerateMethodException(String msg){
        this.msg = msg;
    }
    
    @Override
    public String toString() {
        return "**************** AccelerateMethodException - " + "msg = " + msg + " ****************";
    }
}

public interface Driveable {
    void accelerate(int value) throws AccelerateMethodException; 
    void brake(); 
    void turn(); 
}
