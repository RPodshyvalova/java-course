package lesson03.task2;

public class WorkWithStringDemo {
    public static void main(String args[]){
        int i;
        long startTime;
        long endTime;
        long timeForString;
        long timeForStringBuilder;
        long timeForStringBuffer;
        
        String string = new String();
        StringBuilder stringBuilder = new StringBuilder();
        StringBuffer stringBuffer = new StringBuffer();
        
        startTime = System.currentTimeMillis();
        for(i=0; i<100000; i++)
            string.concat("Hello, friends :)");
        endTime = System.currentTimeMillis();
        timeForString = endTime - startTime;
        
        startTime = System.currentTimeMillis();
        for(i=0; i<100000; i++)
            stringBuilder.append("Hello, friends :)");
        endTime = System.currentTimeMillis();
        timeForStringBuilder = endTime - startTime;
        
        startTime = System.currentTimeMillis();
        for(i=0; i<100000; i++)
            stringBuffer.append("Hello, friends :)");
        endTime = System.currentTimeMillis();
        timeForStringBuffer = endTime - startTime;
        
        System.out.println("Test Performance for the methods: concat of String,  append of StringBuilder, "
                           + "append of StringBuffer.");
        System.out.println("Condition of the problem: concatenate the string \"Hello, friends :)\" 100000 times");
       
       
        System.out.println("Results are: ");
        for(i=0; i<76; i++)
            System.out.print("-");
        System.out.printf("\n|%20s |  %20s |  %20s |\n", "concat -> String","append -> StringBuilder","append -> StringBuffer");
        System.out.printf("|%20s |  %23s |  %22s |\n", timeForString + " ms", timeForStringBuilder + " ms", timeForStringBuffer + " ms");
        for(i=0; i<76; i++)
            System.out.print("-");
        System.out.println();
    }
}
