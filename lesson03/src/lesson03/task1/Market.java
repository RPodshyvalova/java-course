package lesson03.task1;

import java.util.Comparator;

public abstract class Market implements Comparable {
    abstract void deliveryOfTheProduct();
    abstract void  saleOfTheProduct();
    int productCode;
    String productName;
    int producAlltCount;
    int producCurrentCount;
    float productPrice;
       
    Market() {
        productCode = 0;
        productName = "";
        producAlltCount = 0;
        producCurrentCount = 0;
        productPrice = 0.0F; 
    } 
    
      Market(int productCode, String productName, int producAlltCount, float productPrice) {
        this.productCode = productCode;
        this.productName = productName;
        this.producAlltCount = producAlltCount;
        this.producCurrentCount = producAlltCount;
        this.productPrice = productPrice; 
    } 

   @Override
    public int compareTo(Object o) {
        float result;
        Market marketObject = (Market)o;
        
        result = this.productCode - marketObject.productCode;
        if (result != 0){
            return (int)(result/Math.abs(result));
        }
        result = this.productName.compareTo(marketObject.productName);
        if (result != 0){
            return (int)(result/Math.abs(result));
        }
        result = this.producAlltCount - marketObject.producAlltCount;
        if (result != 0){
            return (int)(result/Math.abs(result));
        }
        result = this.producCurrentCount - marketObject.producCurrentCount;
        if (result != 0){
            return (int)(result/Math.abs(result));
        }
        result = this.productPrice - marketObject.productPrice;
        return (result != 0) ? (int)(result/Math.abs(result)) : 0; 
    }
    
    public static Comparator<Market> ComparartorByPrice = new Comparator<Market>() {
        @Override
        public int compare(Market o1, Market o2) {
            float result;
            result = o1.productPrice - o2.productPrice;
            return (result != 0) ? (int)(result/Math.abs(result)) : 0;     
        }
    };
    
    public static Comparator<Market> ComparartorByCode = new Comparator<Market>() {
        @Override
        public int compare(Market o1, Market o2) {
            float result;
            result = o1.productCode - o2.productCode;
            return (result != 0) ? (int)(result/Math.abs(result)) : 0;     
        }
    };
    
    @Override
    public String toString() {
        return "Product = " + productName  + "; "+ "price = " + productPrice+ "UAH; ";
    }
    
}
