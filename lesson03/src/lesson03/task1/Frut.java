package lesson03.task1;

public class Frut extends Market {
    private int calorieProduct;
    
    Frut() {
        super();
        calorieProduct = 0;
    }
    
    Frut(int productCode, String productName, int producAlltCount, float productPrice, int calorieProducts) {
        super(productCode, productName, producAlltCount, productPrice);
        this.calorieProduct = calorieProducts;
    }

    @Override
    public int compareTo(Object o) {
        int result;
        Frut frutObject = (Frut)o; 
        super.compareTo(o);
        result = this.calorieProduct  - frutObject.calorieProduct;
        return (result != 0) ? (int)(result/Math.abs(result)) : 0; 
    }
    
    @Override
    void deliveryOfTheProduct() {
        System.out.println("Delivery of the fruts");
    }

    @Override
    void saleOfTheProduct() {
        System.out.println("Sale of the fruts");
    }
    
    @Override
    public String toString() {
        return  super.toString() + "calorie = "+ this.calorieProduct + "kKal.";
    }

}
