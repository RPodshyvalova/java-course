package lesson03.task1;

public class SortingMethods {

    public static void bubbleSort(Comparable arr[], String  sortingDirection) {
        int length = arr.length-1; 
        boolean flag = true;  
        Comparable tmp;
        while(flag){
            flag = false;
            for(int i=0; i<length; i++){
                if(arr[i+1].compareTo(arr[i]) < 0) {
                    tmp = arr[i];
                    arr[i] = arr[i+1];
                    arr[i+1] = tmp;
                    flag = true; 
                }
            }
            length--; 
        } 
        if(sortingDirection.equals("descending")) {
            reverseArray(arr); 
        }
    } 
    
    public static void selectionSort(Comparable arr[], String  sortingDirection) {
        int iMin;
        Comparable min;
        for(int i=0; i<arr.length-1; i++) {
            min = arr[i];
            iMin = i;
            for(int j=i+1; j<arr.length; j++) {
                if(arr[j].compareTo(min) < 0) {
                    min = arr[j];
                    iMin = j;
                }
            }
            arr[iMin] = arr[i];
            arr[i] = min;
        }
        if(sortingDirection.equals("descending")) {
            reverseArray(arr); 
        }
    }
   
     public static void insertionSort(Comparable arr[], String  sortingDirection) {
        Comparable tmp;
        for(int i=1; i<arr.length; i++) {
            tmp = arr[i];
            int j = 0;
            while(arr[j].compareTo(tmp) < 0) {
                j++;
            }
            for(int k=i-1; k>=j; k--) {
                arr[k+1] = arr[k];
            }
            arr[j] = tmp;
        }   
        if(sortingDirection.equals("descending")) {
            reverseArray(arr); 
        }
    }
    
    public static void reverseArray(Object arr[]) {
        Object temp;
        for(int i=0; i<arr.length/2; i++) {
            temp = arr[i]; 
            arr[i] = arr[arr.length-i-1];
            arr[arr.length-i-1] = temp;
        }
    }
}
