package lesson03.task1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class MainDemo {
    private static Object[] sortingArrayDemo(Object[] arr, int sortingMethod, String sortingDirection) {
        Object copyArray[] = Arrays.copyOf(arr, arr.length);
        switch(sortingMethod) {
            case 1: 
                SortingMethods.bubbleSort((Comparable[]) copyArray, sortingDirection);
                break;
                
            case 2: 
                SortingMethods.selectionSort((Comparable[]) copyArray, sortingDirection);
                break;
                
            case 3: 
                SortingMethods.insertionSort((Comparable[]) copyArray, sortingDirection);
                break;        
        }
        return copyArray;
    }
    
    private static void printArray(Object arr[]){
      for(int i=0; i<arr.length; i++) {
           System.out.println(String.valueOf(i+1) + ": " + arr[i]);
        }
    }
    
    public static void main(String args[]) throws IOException {
        BufferedReader reader;
        String inputValue;
        Object copyArray[];
        
        Frut[] frutArray = new Frut[5];
        frutArray[0] = new Frut(111, "Apples", 30, 12, 46);
        frutArray[1] = new Frut(117, "Grapes", 30, 25, 70);
        frutArray[2] = new Frut(113, "Bananas", 50, 19, 148);
        frutArray[3] = new Frut(119, "Strawberries", 20, 35, 39);
        frutArray[4] = new Frut(115,"Orange", 50, 18, 81);
        
        MusicCDs[] musicCDsArray = new  MusicCDs[5];
        musicCDsArray[0] = new MusicCDs(1214, "Armin van Buuren - A state of trance 631.", 100, 25, "Trance" , 45);
        musicCDsArray[1] = new MusicCDs(1218, "Francia Jazzline Orchestra - Jazz Astoria.", 100, 25, "Jazz" , 30);
        musicCDsArray[2] = new MusicCDs(1212, "Matt Lange  - So Cliche", 100, 26, "House" , 35);
        musicCDsArray[3] = new MusicCDs(1215, "Collection - Lounge Relief Easy Lounge Soiree.", 100, 28, "Lounge" , 40);
        musicCDsArray[4] = new MusicCDs(1216, "Collection - Antonio Vivaldi.", 100, 23, "Classical" , 45);
        
        for(;;){
            reader = new BufferedReader(new InputStreamReader(System.in));    
            System.out.print("\nHi. You are in the supermarket. Which product are you interested in? Choose: "
                            + "\n1 - for fruts \n2 - for mMusic CDs \n3 - for exit\n");
            inputValue = reader.readLine();
            if(!inputValue.equals("3")) {
                if(inputValue.equals("1")) { 
                    printArray(frutArray);
                    System.out.print("\nChoose: "
                                    + "\n11 - sort array of fruts by bubbleSort method by ascending"
                                    + "\n12 - sort array of fruts by bubbleSort method by descending"
                                    + "\n13 - sort array of fruts by selectionSort method by ascending"
                                    + "\n14 - sort array of fruts by insertionSort method by descending"
                                    + "\n15 - for exit\n");
                    inputValue = reader.readLine();
                    System.out.println();
                    switch(inputValue) {
                        case "11": 
                            copyArray = sortingArrayDemo(frutArray, 1, "ascending") ;
                            System.out.println();
                            System.out.println("Array is sorted by the bubble method by ascending");
                            printArray(copyArray);
                            break;
                            
                        case "12": 
                            copyArray = sortingArrayDemo(frutArray, 1, "descending");
                            System.out.println();
                            System.out.println("Array is sorted by the bubble method by descending");
                            printArray(copyArray);
                            break;
                            
                        case "13": 
                            copyArray = sortingArrayDemo(frutArray, 2, "ascending");
                            System.out.println();
                            System.out.println("Array is sorted by the selection method by ascending");
                            printArray(copyArray);
                            break;   
                            
                        case "14": 
                            copyArray = sortingArrayDemo(frutArray, 3, "descending");
                            System.out.println();
                            System.out.println("Array is sorted by the insertion method by descending");
                            printArray(copyArray);
                            break;   
                            
                        case "15": 
                            System.out.println("\nThanks for visit!"); 
                            System.exit(0);
                                
                    }
                }
                if(inputValue.equals("2")) { 
                    printArray(musicCDsArray);
                    System.out.print("\nChoose: "
                                    + "\n21 - sort array of music CDs by bubbleSort method by ascending"
                                    + "\n22 - sort array of music CDs by bubbleSort method by descending"
                                    + "\n23 - sort array of music CDs by selectionSort method by ascending"
                                    + "\n24 - sort array of music CDs by insertionSort method by descending"
                                    + "\n25 - for exit\n");
                    inputValue = reader.readLine();
                    System.out.println();
                    switch(inputValue) {
                        case "21": 
                            copyArray = sortingArrayDemo(musicCDsArray, 1, "ascending") ;
                            System.out.println();
                            System.out.println("Array is sorted by the bubble method by ascending");
                            printArray(copyArray);
                            break;
                            
                        case "22": 
                            copyArray = sortingArrayDemo(musicCDsArray, 1, "descending");
                            System.out.println();
                            System.out.println("Array is sorted by the bubble method by descending");
                            printArray(copyArray);
                            break;
                            
                        case "23": 
                            copyArray = sortingArrayDemo(musicCDsArray, 2, "ascending");
                            System.out.println();
                            System.out.println("Array is sorted by the selection method by ascending");
                            printArray(copyArray);
                            break;   
                            
                        case "24": 
                            copyArray = sortingArrayDemo(musicCDsArray, 3, "descending");
                            System.out.println();
                            System.out.println("Array is sorted by the insertion method by descending");
                            printArray(copyArray);
                            break;   
                            
                        case "25": 
                            System.out.println("\nThanks for visit!"); 
                            System.exit(0);
                                 
                    }
                }
            }else {
                System.out.println("\nThanks for visit!"); 
                System.exit(0);
            }
            
           
//            Market tmp[] = Arrays.copyOf(frutArray, frutArray.length);
//            Arrays.sort(tmp, Market.ComparartorByPrice);
//            printArray(tmp);
        }
       
    }    
}
