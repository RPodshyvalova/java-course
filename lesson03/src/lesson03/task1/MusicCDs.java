package lesson03.task1;

public class MusicCDs extends Market {
    private String genre;
    private int numberOfTracks;

    MusicCDs() {
        super();
        numberOfTracks = 0;
        genre= "";
    }
    
    MusicCDs(int productCode, String productName, int producAlltCount, float productPrice, String genre, int numberOfTracks) {
        super(productCode, productName, producAlltCount, productPrice);
        this.genre = genre;
        this.numberOfTracks = numberOfTracks;
    }
   
    @Override
    public int compareTo(Object o) {
        int result;
        MusicCDs musicCDsObject = (MusicCDs)o; 
        super.compareTo(o);
        
        result = this.numberOfTracks - musicCDsObject.numberOfTracks;
        if (result != 0){
            return (int)(result/Math.abs(result));
        }
        result = this.genre.compareTo(musicCDsObject.genre);
        return (result != 0) ? (int)(result/Math.abs(result)) : 0; 
    }
        
    @Override
    void deliveryOfTheProduct() {
        System.out.println("Delivery of the CDs");
    }

    @Override
    void saleOfTheProduct() {
        System.out.println("Sale of the CDs");
    }   

    @Override
    public String toString() {
        return super.toString() + "genre = " + genre + "; tracks = " + numberOfTracks + ".";
    }

}
