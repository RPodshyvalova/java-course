package taskManager;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class SortedTaskManager implements TaskManager {
    private static Map<Date, Task> registry = new TreeMap<>();

    public void addTask(Date date, Task task) {
        registry.put(date, task);
    }

    public void removeTask(Date date) {
        registry.remove(date);
    }

    //TODO: Rewrite using streams
    public Set<String> getCategories() {
        Set<String> result = new HashSet<>();
//        for (Date date : registry.keySet()) {
//            Task task = registry.get(date);
//            result.add(task.getCategory());
//        }
        registry.keySet().stream()
            .forEach((Date key) -> {
                Task task = registry.get(key);
                result.add(task.getCategory());
            });
        return result;
    }

    //TODO: Rewrite using streams
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> map = new HashMap<>();

//        for (Date date : registry.keySet()) {
//            Task task = registry.get(date);
//
//            String category = task.getCategory();
//
//            if (map.get(category) == null) {
//                map.put(category, new ArrayList<Task>());
//            }
//
//            map.get(category).add(task);
//        }
        registry.keySet().stream()
            .forEach(key -> {
                Task task = registry.get(key);
                String category = task.getCategory();
                if (map.get(category) == null) {
                    map.put(category, new ArrayList<Task>());
                }
                map.get(category).add(task);
            });
        return map;
    }

    //TODO: Rewrite using streams
    public List<Task> getTasksForToday() {
        Calendar today = Calendar.getInstance();
        List<Task> result = new ArrayList<>();

//        for (Date date : registry.keySet()) {
//            Calendar calendar = Calendar.getInstance();
//            calendar.setTime(date);
//
//            if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
//                calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
//
//                result.add(registry.get(date));
//            }
//        }
        registry.keySet().stream()
            .forEach(key -> { 
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(key);

                if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR) &&
                calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)) {
                    result.add(registry.get(key));
                }       
        });
        
        return result;
    }

    public Predicate<Task> outdatedTasksList() {
        return (Task t) -> t.getDate().before(new Date());
    }
    
    public Predicate<Task> actualTasksList() {
        return (Task t) -> t.getDate().after(new Date());
    }
    
    public List<Task> filterTasks(Predicate<Task> predicate){
        return registry.values().stream()
                .filter(predicate)
                .collect(Collectors.<Task>toList());
    }
     
    public void outdatedTasksListDemo(){
        List<Task> result = new ArrayList<>();
        result = filterTasks(outdatedTasksList()); 
        result.forEach((Task t) -> {
            System.out.println("Outdated task " + t.getDescription());
        });
    }
    
    //TODO: Add List<Task> filterTasks(Predicate<Task> predicate)
    //TODO: Read about String.format

    /*
        Sort folders inside specified folder by size descending
        using Files.walkFileTree
     */
}
