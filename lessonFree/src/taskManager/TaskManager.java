package taskManager;

import java.util.*;

public interface TaskManager {
    public void addTask(Date date, Task task);
    public void removeTask(Date date);
    public Set<String> getCategories();
    public Map<String, List<Task>> getTasksByCategories();
    public List<Task> getTasksForToday();
    
}
