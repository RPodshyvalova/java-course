package taskManager;

import java.util.Date;

public class Task {
    private Date date;
    private String category;
    private String description;

    public Task(String category, String description) {
        this.category = category;
        this.description = description;
    }

    public Task(Date date, String category, String description) {
        this.date = date;
        this.category = category;
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }
}
