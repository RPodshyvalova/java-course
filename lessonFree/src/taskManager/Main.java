package taskManager;
        
import java.util.*;



public class Main {
    public static void taskManagerExample() {
        SortedTaskManager taskManager = new SortedTaskManager();

        Calendar cal = Calendar.getInstance();
/*        
        cal.set(2013, Calendar.NOVEMBER, 6, 7, 30);
        taskManager.addTask(cal.getTime(), new Task("sport", "Morning exercises"));

        cal.set(Calendar.HOUR, 8);
        cal.set(Calendar.MINUTE, 0);
        taskManager.addTask(cal.getTime(), new Task("misc", "Shower"));

        cal.set(Calendar.HOUR, 8);
        cal.set(Calendar.MINUTE, 20);
        taskManager.addTask(cal.getTime(), new Task("misc", "Eating"));

        cal.set(Calendar.HOUR, 9);
        cal.set(Calendar.MINUTE, 0);
        taskManager.addTask(cal.getTime(), new Task("work", "Team meeting"));

        cal.set(Calendar.HOUR, 9);
        cal.set(Calendar.MINUTE, 30);
        taskManager.addTask(cal.getTime(), new Task("work", "Working"));
*/
        cal.set(2013, Calendar.NOVEMBER, 6, 7, 30);
        taskManager.addTask(cal.getTime(), new Task(cal.getTime(), "sport", "Gym"));

        cal.set(2015, Calendar.NOVEMBER, 6, 7, 30);
        taskManager.addTask(cal.getTime(), new Task(cal.getTime(), "education", "Scala programming"));

        System.out.println("Categories so far: " + taskManager.getCategories());

//       System.out.println("Tasks for today:");
//        for (Task task : taskManager.getTasksForToday()) {
//            System.out.println(task.getDescription());
//        }
        taskManager.getTasksForToday().stream()
            .forEach((Task t) -> { 
                System.out.println("Task for today:" + t.getDescription());}
            );
        
        Map<String,List<Task>> tasksByCategories = taskManager.getTasksByCategories();
//        for (String category : tasksByCategories.keySet()) {
//            System.out.println(category);
//
//            for (Task task : tasksByCategories.get(category)) {
//                System.out.println("    " + task.getDescription());
//            }
//        }
        tasksByCategories.keySet().stream()
            .forEach((String key) -> {
                System.out.println("Category " + key);
                tasksByCategories.get(key).stream()
                    .forEach((Task t) -> {
                        System.out.println(t.getDescription());
                    });
            });
       
        /**
         * 
         */
        
        taskManager.outdatedTasksListDemo();
       
    }

    public static void main(String[] args) {
       //streamExample();
        taskManagerExample();
    }

    public static void lambdaExample() {
        List<Task> tasks = new ArrayList<>();

        tasks.add(new Task(new Date(2014, 3, 20, 9, 30), null, null));
        tasks.add(new Task(new Date(2014, 3, 20, 7, 30), null, null));
        tasks.add(new Task(new Date(2014, 3, 20, 8, 30), null, null));

        Collections.sort(tasks, new Comparator<Task>() {
            @Override
            public int compare(Task o1, Task o2) {
                return o1.getDate().compareTo(o2.getDate());
            }
        });

        Collections.sort(tasks, (o1, o2) -> {
            System.out.println("");
            return o1.getDate().compareTo(o2.getDate());
        });

        for (Task task : tasks) {
            System.out.println(task.getDate());
        }
    }

    public static void streamExample() {
        List<String> strings = Arrays.asList("56", "44", "1");
        List<Integer> integers = new ArrayList<>();

        for (String string : strings) {
            int i = Integer.parseInt(string);


            if (i % 2 == 0) {
                integers.add(i);
            }
        }

        strings.stream()
            .map((string) -> {
                System.out.println("Transforming " + string + " into integer");
                return Integer.parseInt(string);
            })
            .filter((n) -> {
                boolean passed = n % 2 == 0;
                System.out.println("Integer " + n + (passed ? "" : " not") +
                        " passed filter");
                return passed;
            })
            .limit(10)
            .forEach((integer) -> {
                System.out.println(integer);
            });
       
        /**
         * Использование интерфейсов возможно двумя способами:
         * 1)Класс может реализовывать интерфейс. Реализация интерфейса заключается в том, что в описании класса данный интерфейс указывается как реализуемый, а в коде класса обязательно определяются все методы, которые описаны в интерфейсе, в полном соответствии с сигнатурами из описания этого интерфейса. То есть, если класс реализует интерфейс, для любого экземпляра этого класса существуют и могут быть вызваны все описанные в интерфейсе методы. Один класс может реализовать несколько интерфейсов одновременно.
         * 2)Возможно объявление переменных и параметров методов как имеющих тип-интерфейс. В такую переменную или параметр может быть записан экземпляр любого класса, реализующего интерфейс. Если интерфейс объявлен как тип возвращаемого значения функции, это означает, что функция возвращает объект класса, реализующего данный интерфейс.
         */
          
        IntOperation sum = (i1, i2) -> i1 + i2;
        IntOperation minus = (i1, i2) -> i1 - i2;
        IntOperation div = (i1, i2) -> i1 / i2;
        IntOperation mul = (i1, i2) -> i1 * i2;

        System.out.println(doOperation(5, 10, sum));
        System.out.println(doOperation(5, 10, minus));
        System.out.println(doOperation(5, 10, div));
        System.out.println(doOperation(5, 10, (i1, i2) -> i1 % i2));
    }

    public static int doOperation(int a, int b, IntOperation operation) {
        return operation.operate(a, b);
    }

    @FunctionalInterface
    public static interface IntOperation {
        public int operate(int i1, int i2);
    }
}
