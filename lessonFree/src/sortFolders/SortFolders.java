package sortFolders;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;



public final class SortFolders extends SimpleFileVisitor<Path>{
    private static Map<Path, Long> map = new TreeMap<>();
    private static ArrayList<Path> rootSubDirectoriesList = new ArrayList<>(); 
    private static long dirSize = 0L;

    private static class CalculateDirectorySize extends SimpleFileVisitor<Path> {
        @Override 
        public FileVisitResult visitFile(Path file, BasicFileAttributes attributes) throws IOException {
            dirSize += attributes.size();
            return FileVisitResult.CONTINUE;
        }
    }
   
    public static void getRootSubDirectories(Path path){
        if (Files.isDirectory(path)) {
            try (DirectoryStream<Path> stream = Files.newDirectoryStream(path)) {
                for (Path p : stream) {
                    if( Files.isDirectory(p) ){
                        rootSubDirectoriesList.add(p);
                    }
                }
            } catch (IOException e) {
                e.getMessage(); 
            }
        }
   }
    
    private static boolean testPath(Path path) {
        return (Files.isDirectory(path)) ? true : false;
    }
    
    public static Map sortMapByValue(Map map) {	 
	List list = new LinkedList(map.entrySet());
        Collections.sort(list, (o1 ,o2) -> {
            return ((Comparable) ((Map.Entry) (o2)).getValue())
                .compareTo(((Map.Entry) (o1)).getValue());
	});
 
	Map sortedMap = new LinkedHashMap();
        list.stream()
            .forEach((p) -> {
                Map.Entry entry = (Map.Entry) p;
                sortedMap.put(entry.getKey(), entry.getValue());
        });
	return sortedMap;
    }
    
    public static void main(String args[]) throws IOException{
        System.out.println("Hi! Please, input path:");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
       
        while (!"exit".equals(command)) {
            if (testPath(Paths.get(command))){
                getRootSubDirectories(Paths.get(command)); 
                FileVisitor<Path> fileProcessor = new CalculateDirectorySize();
               
                rootSubDirectoriesList.stream()
                    .forEach((Path p)  -> {
                        try {
                            Files.walkFileTree(p, fileProcessor);
                        } catch (IOException e) {
                            e.getMessage();
                        }
                        map.put(p, dirSize);
                        dirSize = 0L;  
                    });
                
                map = sortMapByValue(map);
                
                map.entrySet().stream()
                    .forEach((Map.Entry<Path, Long> entry) -> {
                        System.out.println(String.format("%15d bytes \tDirectory : '%s'", 
                            entry.getValue(), entry.getKey().toString()));
                    });

                rootSubDirectoriesList.clear();
                map.clear();
                
                System.out.println("\n\nI expect from you next enter... or type the word exit for exit:");
            } else  {
                System.out.println("Wrong path. Please try again or type the word exit for exit:");
            }
            command = scanner.next();
        }
    }
}
