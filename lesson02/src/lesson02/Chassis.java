package lesson02;

import java.util.ArrayList;
import java.util.Objects;

public class Chassis {
    ArrayList<Wheel> wheels;
    int numbersOfWeels;
    
    Chassis(){
        numbersOfWeels = 0;
        wheels = null;
    }
    
    Chassis(int numbersOfWeels, Wheel wheel) throws CloneNotSupportedException{
        wheels = new  ArrayList<Wheel>();
        for(int i=0; i<numbersOfWeels; i++){
          wheels.add((Wheel)wheel.clone());
        }
    }

    public ArrayList<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(ArrayList<Wheel> wheels) {
        this.wheels = wheels;
    }

    public int getNumbersOfWeels() {
        return numbersOfWeels;
    }

    public void setNumbersOfWeels(int numbersOfWeels) {
        this.numbersOfWeels = numbersOfWeels;
    }

    @Override
    public String toString() {
       String allWheelsToString="";
       for(Wheel i : wheels ){
           allWheelsToString+=i.toString()+"\n";
       } 
       return "Chassis{" + "wheels=" + allWheelsToString + ", numbersOfWeels=" + numbersOfWeels + '}';
    }
    
   /* 
    public static void main(String args[]) throws CloneNotSupportedException{
        Chassis c = new Chassis(4, new Wheel("wheel", 17, "iron", 1, " Michelin"));
        System.out.println(c.toString());
    }
   */
}

