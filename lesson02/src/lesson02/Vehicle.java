package lesson02;

import java.util.Objects;

public abstract class Vehicle implements Driveable{
    abstract void driveSomeKm(int valueKm, int speed);
    abstract void increaseEnergy(Float value);
    abstract String showBalanceEnergy();
    abstract String showCurrentSpeed();
    private String model;
    private int currentSpeed;
    private int serialNumber;
    private String manufacturer;
    private int yearOfManufacture;
    
    Vehicle(){
       model = "";
       currentSpeed = 0;
       serialNumber = 0;
       manufacturer = "";
       yearOfManufacture = 0;
    }
    
    Vehicle(String model, int currentSpeed, int serialNumber, String manufacturer, int yearOfManufacture){
       this.model = model;
       this.currentSpeed = currentSpeed;
       this.serialNumber = serialNumber;
       this.manufacturer = manufacturer;
       this.yearOfManufacture = yearOfManufacture;
    }

    public String getModelOfCar() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(int currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public int getYearOfManufacture() {
        return yearOfManufacture;
    }

    public void setYearOfManufacture(int yearOfManufacture) {
        this.yearOfManufacture = yearOfManufacture;
    }

    @Override
    public String toString() {
        return "Vehicle{" + "model=" + model + ", currentSpeed=" + currentSpeed 
                + ", serialNumber="  + serialNumber + ", manufacturer=" + manufacturer 
                + ", yearOfManufacture=" + yearOfManufacture + '}';
    }
 }
