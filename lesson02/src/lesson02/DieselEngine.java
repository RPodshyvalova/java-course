package lesson02;

import java.util.Objects;

public class DieselEngine implements ForceProvider {
    private String model;
    private int power;
    private int maxSpeed;
    private float fuelConsumption;
    private float accelerationTo100;
    private int serialNumber;
    private String manufacturer;

    DieselEngine(){
        model="";
        power=0;
        maxSpeed=0;
        fuelConsumption=0.0F;
        accelerationTo100=0.0F;
        serialNumber=0;
        manufacturer="";
    }
    
    DieselEngine(int power, int maxSpeed, float fuelConsumption, float accelerationTo100){
        this();
        this.power=power;
        this.maxSpeed=maxSpeed;
        this.fuelConsumption=fuelConsumption;
        this.accelerationTo100=accelerationTo100;
    }
    
    DieselEngine(String model, int power, int maxSpeed, float fuelConsumption, float accelerationTo100, int serialNumber, String manufacturer){
        this.model= model;
        this.power=power;
        this.maxSpeed=maxSpeed;
        this.fuelConsumption=fuelConsumption;
        this.accelerationTo100=accelerationTo100;
        this.serialNumber=serialNumber;
        this.manufacturer=manufacturer;
    }

    @Override
    public void run() {
        System.out.println("Diesel engine is runing");
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public float getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public float getAccelerationTo100() {
        return accelerationTo100;
    }

    public void setAccelerationTo100(float accelerationTo100) {
        this.accelerationTo100 = accelerationTo100;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "DieselEngine{" + "model=" + model + ", power=" + power + ", maxSpeed=" + maxSpeed 
                + ", fuelConsumption=" + fuelConsumption + ", accelerationTo100=" + accelerationTo100 
                + ", serialNumber=" + serialNumber + ", manufacturer=" + manufacturer + '}';
    }
    
}
