package lesson02;

import java.util.Objects;

public class Wheel implements ForceAcceptor, Cloneable{
    private String model;
    private float size;
    private String materialMadeOf;
    private int serialNumber;
    private String manufacturer; 
    
    Wheel(){
        model = "";
        size = 0.0F;
        materialMadeOf = "";
        serialNumber = 0;
        manufacturer = ""; 
    }
    
    Wheel(float size){
        this();
        this.size = size;
    }
    
    Wheel(String model, float size, String materialMadeOf, int serialNumber, String manufacturer){
        this.model = model;
        this.size =  size;
        this.materialMadeOf = materialMadeOf;
        this.serialNumber = serialNumber;
        this.manufacturer = manufacturer; 
    }

    @Override
    public void rotate() {
          System.out.println("The wheel is rotating.");
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getSize() {
        return size;
    }

    public void setSize(float size) {
        this.size = size;
    }

    public String getMaterialMadeOf() {
        return materialMadeOf;
    }

    public void setMaterialMadeOf(String materialMadeOf) {
        this.materialMadeOf = materialMadeOf;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "Wheel{" + "model=" + model + ", size=" + size + ", materialMadeOf=" + materialMadeOf 
                + ", serialNumber=" + serialNumber + ", manufacturer=" + manufacturer + '}';
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone(); 
    }
}
