package lesson02;

import java.util.Objects;

public class SolarBattery implements EnergyProvider{
    private String model;
    private float maxCapacity;
    private float currentCapacity;
    private float weight;
    private int serialNumber;
    private String manufacturer;

    
    SolarBattery(){
        model = "";
        maxCapacity = 0.0F;
        currentCapacity = 0.0F;
        weight = 0.0F;
        serialNumber = 0;
        manufacturer = "";
  } 
     
    SolarBattery(String model, float capacity, float currentCapacity, float weight, int serialNumber, String manufacturer){
        this.model = model;
        this.maxCapacity = maxCapacity;
        this.currentCapacity = currentCapacity;
        this.weight = weight;
        this.serialNumber = serialNumber;
        this.manufacturer = manufacturer;
    }

    @Override
    public void fillEnergySource(Float value) {
        if(currentCapacity + value >= maxCapacity){
            System.out.println("The solar battery is loaded.");
        }else {  
            currentCapacity += value;
            System.out.println("The solar battery is loading.");
        }
    }

    @Override
    public void expendEnergySource() {
       System.out.println("The solar battery level is reduced.");
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(float maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public float getCurrentCapacity() {
        return currentCapacity;
    }

    public void setCurrentCapacity(float currentCapacity) {
        this.currentCapacity = currentCapacity;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public int getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(int serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    @Override
    public String toString() {
        return "SolarBattery{" + "model=" + model + ", maxCapacity=" + maxCapacity 
                + "kWh, currentCapacity=" + currentCapacity + "kWh, weight=" + weight 
                + "kg, serialNumber=" + serialNumber + ", manufacturer=" + manufacturer + '}';
    }
}
