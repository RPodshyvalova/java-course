package lesson02;

public interface Driveable {
    void accelerate(int value); 
    void brake(); 
    void turn(); 
}
