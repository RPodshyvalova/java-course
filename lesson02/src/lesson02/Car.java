package lesson02;

import java.util.Objects;

public class Car extends Vehicle{
    private DieselEngine dieselEngine;
    private Chassis chassis; 
    private GasTank gasTank;
    
    Car(){
       super(); 
       dieselEngine = null;
       gasTank = null;
       chassis = null;
    }
    
    Car(String model, int currentSpeed, int serialNumber, String manufacturer, int yearOfManufacture, 
        DieselEngine dieselEngine,Chassis chassis, GasTank gasTank){
       super(model, currentSpeed, serialNumber, manufacturer, yearOfManufacture); 
       this.dieselEngine = dieselEngine;
       this.gasTank = gasTank;
       this.chassis = chassis;
    }

    @Override
    public void driveSomeKm(int valueKm, int speed) {
       if(speed > this.dieselEngine.getMaxSpeed()){
            this.setCurrentSpeed(this.dieselEngine.getMaxSpeed());
        }else{ 
            this.setCurrentSpeed(speed);
        }
       float consumption = this.dieselEngine.getFuelConsumption()*valueKm/100;
       if(  this.gasTank.getCurrentCapacity()-consumption>0){
            this.gasTank.setCurrentCapacity(this.gasTank.getCurrentCapacity()-consumption); 
            System.out.println(this.getModelOfCar()+" drive "+ String.valueOf(valueKm)+" km; with speed is "+this.getCurrentSpeed()
                               +" and left in the tank " + this.gasTank.getCurrentCapacity() +" l");
       }else{
            float canDriveKm = this.gasTank.getCurrentCapacity()*100/this.dieselEngine.getFuelConsumption();
            this.gasTank.setCurrentCapacity(0); 
            System.out.println(this.getModelOfCar()+" drive "+ String.valueOf(canDriveKm)+" km but "+ 
                                                   +valueKm+" of desired; with speed is "+this.getCurrentSpeed()
                                                   +" and left in the tank " + this.gasTank.getCurrentCapacity() +" l"
                                                   +"\nPlease fill the Car for motion.");
       }
    }
    
    @Override
    public void increaseEnergy(Float value) {
       this.gasTank.fillEnergySource(value);
    }

    @Override
    public String showBalanceEnergy() {
       return String.valueOf(this.gasTank.getCurrentCapacity());
    }

    
    @Override
    public String showCurrentSpeed() {
        return String.valueOf(this.getCurrentSpeed());
    }

    @Override
    public void accelerate(int value) {
        if( this.getCurrentSpeed()+value > this.dieselEngine.getMaxSpeed()){
          this.setCurrentSpeed(this.getCurrentSpeed()+value);
        }else {
            this.setCurrentSpeed(this.dieselEngine.getMaxSpeed());
        }
    }

    @Override
    public void brake() {
        System.out.println("Car brake");
    }

    @Override
    public void turn() {
        System.out.println("Car turn");
    }

    public DieselEngine getDieselEngine() {
        return dieselEngine;
    }

    public void setDieselEngine(DieselEngine dieselEngine) {
        this.dieselEngine = dieselEngine;
    }

    public Chassis getChassis() {
        return chassis;
    }

    public void setChassis(Chassis chassis) {
        this.chassis = chassis;
    }

    public GasTank getGasTank() {
        return gasTank;
    }

    public void setGasTank(GasTank gasTank) {
        this.gasTank = gasTank;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

