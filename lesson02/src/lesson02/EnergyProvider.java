package lesson02;

public interface EnergyProvider {
    void fillEnergySource(Float value);
    void expendEnergySource();
    
}
