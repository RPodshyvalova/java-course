package lesson02;

import java.util.Objects;

public class SolarPoweredCar extends Vehicle{
    private ElectricEngine electricEngine;
    private SolarBattery solarBattery;
    private Chassis chassis; 

    
    SolarPoweredCar(){ 
        super();
        electricEngine = null;
        solarBattery = null;
        chassis = null;
    }
   
    SolarPoweredCar(String model, int currentSpeed, int serialNumber, String manufacturer, int yearOfManufacture, 
                    ElectricEngine electricEngine, SolarBattery solarBattery, Chassis chassis){ 
        super(model, currentSpeed, serialNumber, manufacturer, yearOfManufacture); 
        this.electricEngine = electricEngine;
        this.solarBattery = solarBattery;
        this.chassis = chassis;
    }

    @Override
    void driveSomeKm(int valueKm, int speed) {
        System.out.println("Solar Powered Car drive some km");
    }

    @Override
    void increaseEnergy(Float value) {
       this.solarBattery.fillEnergySource(value);
    }

    @Override
    String showBalanceEnergy() {
       return String.valueOf(this.solarBattery.getCurrentCapacity());
    }

    @Override
    String showCurrentSpeed() {
        return String.valueOf(this.getCurrentSpeed());
    }

    @Override
    public void accelerate(int value) {
        if( this.getCurrentSpeed()+value > this.electricEngine.getMaxSpeed()){
          this.setCurrentSpeed(this.getCurrentSpeed()+value);
        }else {
            this.setCurrentSpeed(this.electricEngine.getMaxSpeed());
        }
    }

    @Override
    public void brake() {
        System.out.println("Solar powered car brake");
    }

    @Override
    public void turn() {
        System.out.println("Solar powered car turn");
    }

    @Override
    public String toString() {
        return super.toString()+ "SolarPoweredCar{" + "electricEngine=" + electricEngine 
               + ", solarBattery=" + solarBattery + ", chassis=" + chassis + '}';
    }
 
}
