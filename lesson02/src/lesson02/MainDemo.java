package lesson02;

public class MainDemo {

    public static void main(String[] args) throws CloneNotSupportedException {
        System.out.println("---------------------------------------------------------------------------------------------------------"); 
        Vehicle[] vehicle = new Vehicle[3];
        
        vehicle[0] = new Car("Bentley Continental Supersports", 140, 1, "Bentley Motors Ltd.", 2010, 
                              new DieselEngine(621, 329, 16.3F, 3.9F), 
                              new Chassis(4, new Wheel("wheel", 18, "Carbon steel", 1, " BBS")), 
                              new GasTank(90F, 90F));
        vehicle[1] = new SolarPoweredCar();
        vehicle[2] = new Boat();

        for(int i = 0; i < vehicle.length; i++){
            System.out.println(vehicle[i].toString()+"\n");
        }
        System.out.println("---------------------------------------------------------------------------------------------------------"); 
        System.out.println("In the gas tank of the "+ vehicle[0].getModelOfCar()+" is "+vehicle[0].showBalanceEnergy()+" l now");
        vehicle[0].driveSomeKm(200, 200);
        //vehicle[0].driveSomeKm(2200, 400);
        vehicle[0].increaseEnergy(10F);
        System.out.println("In the gas tank of the "+ vehicle[0].getModelOfCar()+" is "+vehicle[0].showBalanceEnergy()+" l now");
        System.out.println("---------------------------------------------------------------------------------------------------------"); 
    }    
}
