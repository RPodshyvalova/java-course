package lesson04.task2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainDemo {
    public static void main(String args[]) throws ParseException{
        ImplementationTaskManager manager = new ImplementationTaskManager();
        String tmp;
        SimpleDateFormat dt = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        
        try {
            manager.addTask(dt.parse("01.03.2015 07:00"), new Task("Study", "Boil a cup of coffee."));
        } catch (InvalidDateTasksException e) {
            System.out.println(e.toString());
        }
        try {
            manager.addTask(dt.parse("01.03.2015 07:15"), new Task("Study", "Begin work on the project for GeekHub."));
        } catch (InvalidDateTasksException e) {
            System.out.println(e.toString());
        }
        try {
            manager.addTask(dt.parse("19.11.2014 19:10"), new Task("Personal", "Go to the cinema Lyubava to the film Interstellar."));
        } catch (InvalidDateTasksException e) {
           System.out.println(e.toString());
        }
        try {
            manager.addTask(dt.parse("22.01.2016 07:00"), new Task("Personal", "Making the trip to the Svalbard archipelago."));
        } catch (InvalidDateTasksException e) {
            System.out.println(e.toString());
        }
        try {
            manager.addTask(dt.parse("01.04.2015 07:00"), new Task("Study", "Complete the project for GeekHub."));
        } catch (InvalidDateTasksException e) {
            System.out.println(e.toString());
        }
        try {
            manager.addTask(dt.parse("15.11.2014 17:49"), new Task("Study", "Write java code."));
        } catch (InvalidDateTasksException e) {
            System.out.println(e.toString());
        }
        try {
            manager.addTask(dt.parse("15.11.2014 17:50"), new Task("Study", "Check java code."));
        } catch (InvalidDateTasksException e) {
            System.out.println(e.toString());
        }
        
        System.out.println("------------------------------------------------------------------------------------");   
        System.out.println("Task manager content: ");
        if (manager.getMap().size() > 0){
            printMap(manager.getMap());
        } else {
            System.out.println("\tNo tasks");
        }
        
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("Categories: ");
        if (manager.getCategories().size() > 0){
            printList(manager.getCategories());
        } else {
            System.out.println("\tNo categories");
        }
        
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("Tasks by category: ");
        tmp = "Study";
        if (manager.getTasksByCategory(tmp).size() > 0){
            printList(manager.getTasksByCategory("Study"));
        } else {
            System.out.println("\tNo task by category " + tmp);
        }
        
        System.out.println("------------------------------------------------------------------------------------");
        System.out.println("Tasks for today: ");
        if (manager.getTasksForToday().size() > 0){
            printList(manager.getTasksForToday());
        } else {
            System.out.println("\tNo tasks for today " + dt.format(new Date()));
        }        
        
        System.out.println("------------------------------------------------------------------------------------");    
        System.out.println("Tasks by categories: ");
        if (manager.getMap().size() > 0){
            printMap(manager.getTasksByCategories());
        } else {
            System.out.println("\tNo tasks");
        }
    }
    
    private static <T> void printList(Collection<T> list ){
        for (T value : list){        
            System.out.println("\t" + value);
        }
    }  
    
    private static <K,V> void printMap( Map<K,V> map ){
        for (Map.Entry<K, V> entry : map.entrySet()){
            System.out.println("\t" + entry.getKey() +" "+ entry.getValue());
        }
    }

}
