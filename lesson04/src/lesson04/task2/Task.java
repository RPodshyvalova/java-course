package lesson04.task2;

public class Task {
    private String category;
    private String description; 
    
    Task(){
        category = "";
        description = ""; 
    }

    Task(String category, String description){
        this.category = category;
        this.description = description; 
    }
      
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Task is: " + "category=" + category + ", description=" + description;
    }

}
