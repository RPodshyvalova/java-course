package lesson04.task2;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;


public class ImplementationTaskManager implements TaskManager{
    Map<Date, Task> map;
 
    ImplementationTaskManager(){
        map = new HashMap<>();
    } 

    public Map<Date, Task> getMap() {
        return map;
    }
    
    @Override
    public void addTask(Date date, Task task) throws InvalidDateTasksException {
        boolean flag = false;
        for (Date tmpDate : map.keySet()) {
            flag = tmpDate.equals(date);
        }
        if (flag) {
            throw new InvalidDateTasksException("At that date already has a task.");
        } else {
            map.put(date, task);
        }
        map = new TreeMap<Date, Task>(map);
   }

    @Override
    public void removeTask(Date date) {
        map.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Collection<String> hashSet = new HashSet<>();
        for (Map.Entry<Date, Task> entry : map.entrySet()) {
            hashSet.add(entry.getValue().getCategory());
        }
        return hashSet;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>>  hashMap = new HashMap<>();
        for (String category : this.getCategories()){
            hashMap.put(category,  this.getTasksByCategory(category));
        }
        return hashMap;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> arrayList = new ArrayList<>();
        for (Map.Entry<Date, Task> entry : map.entrySet()) {
            if (entry.getValue().getCategory().equals(category) ){
                arrayList.add(entry.getValue());
            }
        }
        return arrayList;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> arrayList = new ArrayList<>();
        SimpleDateFormat dt = new SimpleDateFormat("dd.MM.yyyy");
        for (Map.Entry<Date, Task> entry : map.entrySet()) {
            if (dt.format(entry.getKey()).equals(dt.format(new Date()))){
                arrayList.add(entry.getValue());
            }
        }
        return arrayList;
    }
}
