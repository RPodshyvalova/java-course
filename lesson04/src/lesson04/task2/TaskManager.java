package lesson04.task2;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

class  InvalidDateTasksException extends Exception{
    private String msg;
    
    InvalidDateTasksException(){
        msg = null;
    }
    
    InvalidDateTasksException(String msg){
        this.msg = msg;
    }
    
    @Override
    public String toString() {
        return "**************** InvalidDateTasksException - " + "msg = " + msg + " ***************";
    }
}

public interface TaskManager {
    public void addTask(Date date, Task task) throws InvalidDateTasksException;
    public void removeTask(Date date);
    public Collection<String> getCategories();
    public Map<String, List<Task>> getTasksByCategories();
    public List<Task> getTasksByCategory(String category);
    public List<Task> getTasksForToday();
}
