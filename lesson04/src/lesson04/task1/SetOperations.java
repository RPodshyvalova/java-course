package lesson04.task1;

import java.util.Set;

public interface SetOperations {
    public <T> boolean equals(Set<T> a, Set<T> b);
    public <T> Set<T> union(Set<T> a, Set<T> b);
    public <T> Set<T> subtract(Set<T> a, Set<T> b);
    public <T> Set<T> intersect(Set<T> a, Set<T> b);
    public <T> Set<T> symmetricSubtract(Set<T> a, Set<T> b);    
}
