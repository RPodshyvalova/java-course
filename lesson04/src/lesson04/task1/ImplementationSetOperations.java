package lesson04.task1;

import java.util.HashSet;
import java.util.Set;

public class ImplementationSetOperations<T> implements SetOperations{
    private Set tmp;
    private Set tmp2;
    
    ImplementationSetOperations(){
       tmp = new HashSet<T>();
       tmp2 = new HashSet<T>();
    }
    
    @Override
    public <T> boolean equals(Set<T> a, Set<T> b) {
        if (a.getClass() != b.getClass()) {
            return false;
        } else { 
            if (a.size() != b.size()){ 
                return false;
            } else {
                if (!(a.containsAll(b) && b.containsAll(a))){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public <T> Set<T> union(Set<T> a, Set<T> b) {
        tmp.addAll(a);
        if (tmp.addAll(b)){
            return tmp;
        }
        return null;
    }

    @Override
    public <T> Set<T> subtract(Set<T> a, Set<T> b) {
        tmp.addAll(a);
        if (tmp.removeAll(b)){
            return tmp;
        }
        return null;
    }

    @Override
    public <T> Set<T> intersect(Set<T> a, Set<T> b) {
        tmp.addAll(a);
        if (tmp.retainAll(b)){
            return tmp;
        }
        return null;
    }

    @Override
    public <T> Set<T> symmetricSubtract(Set<T> a, Set<T> b) {
        tmp.addAll(a);
        tmp2.addAll(b);
        if (tmp.removeAll(b) && tmp2.removeAll(a)){
            if (tmp.addAll(tmp2)){
             return tmp;
            }
        }
        return null;
    }
}
