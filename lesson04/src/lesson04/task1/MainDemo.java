package lesson04.task1;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

public class MainDemo {

    public static void main(String arggs[]){
        Set<Integer> hashSetInteger1 = new  HashSet<>(); 
        Set<Integer> hashSetInteger2 = new HashSet<>(); 
        Set<Character> hashSetChar1 = new  LinkedHashSet<>(); 
        Set<Character> hashSetChar2 = new  LinkedHashSet<>(); 
        Integer integerValue1, integerValue2;
        int charCode, charCode2;
//          Float floatValue;
        
        for (int i = 0; i < 5; i++){
            integerValue1= new Random().nextInt(10);
            integerValue2= new Random().nextInt(8);
            hashSetInteger1.add(integerValue1);
            hashSetInteger2.add(integerValue2);
             
            charCode = getRandomRange(97, 122);
            charCode2 = getRandomRange(97, 122);
            hashSetChar1.add((char)charCode);
            hashSetChar2.add((char)charCode2);
            
//            floatValue = new Random().nextFloat()*5;
//            hashSetChar1.add(floatValue);
//            hashSetChar2.add(Float.valueOf(floatValue+1));
        }
     
   
        ImplementationSetOperations operation = new ImplementationSetOperations();
        System.out.println("------------------------------------------------------------------------------------");
        System.out.print("First Set<Integer>: ");
        printSet(hashSetInteger1);
        System.out.println();
        System.out.print("Secont Set<Integer>: ");
        printSet(hashSetInteger2);
        System.out.println();
        System.out.println("1) equals union of the sets: " + operation.equals(hashSetInteger1, hashSetInteger2));
        System.out.println("2) union of the sets: " + operation.union(hashSetInteger1, hashSetInteger2));
        System.out.println("3) subtract of the sets: " + operation.subtract(hashSetInteger1, hashSetInteger2));
        System.out.println("4) intersect of the sets: " + operation.intersect(hashSetInteger1, hashSetInteger2));
        System.out.println("5) symmetricSubtract of the sets: " + operation.symmetricSubtract(hashSetInteger1, hashSetInteger2));
   
        ImplementationSetOperations operation2 = new ImplementationSetOperations();
        System.out.println("------------------------------------------------------------------------------------");
        System.out.print("First Set<Character>: ");
        printSet(hashSetChar1);
        System.out.println();
        System.out.print("Secont Set<Character>: ");
        printSet(hashSetChar2);
        System.out.println();
        System.out.println("1) equals union of the sets: " + operation2.equals(hashSetChar1, hashSetChar2));
        System.out.println("2) union of the sets: " + operation2.union(hashSetChar1, hashSetChar2));
        System.out.println("3) subtract of the sets: " + operation2.subtract(hashSetChar1, hashSetChar2));
        System.out.println("4) intersect of the sets: " + operation2.intersect(hashSetChar1, hashSetChar2));
        System.out.println("5) symmetricSubtract of the sets: " + operation2.symmetricSubtract(hashSetChar1, hashSetChar2));
        System.out.println("------------------------------------------------------------------------------------");
    }
    
    public static <T> void printSet(Set<T> set) {
        for(T value : set){        
            System.out.print( "  " + value);
        }
    }
     
    public static int getRandomRange(int a, int b) {
        Random value = new Random();
        int result =  value.nextInt((b + 1) - a) + a;
        return result;
    }
}
